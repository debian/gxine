/*
 * Copyright (C) 2003-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * GtkComboBox with history
 *
 */

#ifndef GXINE_HISTORY_H
#define GXINE_HISTORY_H

typedef void (*history_activate_t) (GtkWidget *, char *);

GtkWidget *history_combo_box_new (history_activate_t);
void history_combo_box_set_activate_button (GtkWidget *cb, GtkWidget *activate);
void history_combo_box_activate (GtkWidget *);
void history_combo_box_clear (GtkWidget *, gpointer);

static inline GtkWidget *
history_combo_box_get_entry (GtkWidget *cb)
{
  return gtk_bin_get_child (GTK_BIN (cb));
}

#endif
