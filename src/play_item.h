/*
 * Copyright (C) 2003-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * playlist item / media mark:
 * an mrl + options (e.g. volume, start time, brightness/contrast ...)
 */

#ifndef GXINE_PLAY_ITEM_H
#define GXINE_PLAY_ITEM_H

#include <glib.h>
#include <stdio.h>
#include <xine/xmlparser.h>

typedef struct play_item_s play_item_t;

typedef enum {
  PLAY_ITEM_NORMAL,
  PLAY_ITEM_AUTOPLAY,
  PLAY_ITEM_BROWSER,
  PLAY_ITEM_PLAYLIST,
} play_item_type;

struct play_item_s {

  char   *title;
  char   *mrl;

  int	  start_time, duration;
  struct {
    int offset;
    gboolean set;
  } av, spu;

  GList  *options; /* options are simply script engine command strings */
  play_item_type type;

  gboolean untitled; /* the title was extracted from the MRL */
  gboolean played; /* this has been played in playlist */
  gboolean ignore; /* auto-play to ignore for some reason, e.g. auth failure */
};

play_item_t *play_item_new (const char *title, const char *mrl,
			    int start_time, int duration);
void play_item_dispose (play_item_t *play_item);

void play_item_add_option (play_item_t *item, const char *option);

char *play_item_xml (const play_item_t *item, int depth);
play_item_t *play_item_load (xml_node_t *xplay_item);

play_item_t *play_item_copy (play_item_t *item);

void play_item_play (play_item_t *play_item,
		     int pos, int pos_time, int duration);

/* gui part */

void play_item_init (void);

/* return value: 0=> cancel selected, 1=> ok selected */
typedef enum {
  PLAY_ITEM_LIST,
  PLAY_ITEM_MEDIAMARK,
  PLAY_ITEM_LIST_NEW,
  PLAY_ITEM_MEDIAMARK_NEW,
} play_item_type_t;
int  play_item_edit (play_item_t *item, play_item_type_t type,
		     const char *title, GtkWidget *parent);

void clip_set_play_item_from_selection (GtkWidget *);
play_item_t *clip_get_play_item (void);

#endif
