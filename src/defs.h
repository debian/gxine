/*
 * Copyright (C) 2001-2006 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * misc. #defines
 */

#ifndef GXINE_DEFS_H
#define GXINE_DEFS_H

#define GXINE_GTK_COMPAT

/* GTK versioning */
#ifdef GXINE_GTK_COMPAT
# define gxine_gtk_check_version(major,minor,micro) \
    gtk_check_version ((major), (minor), (micro))
# define GTK_HAVE(func,major,minor,micro) (func)
#else
# define gxine_gtk_check_version(major,minor,micro) \
    (GTK_CHECK_VERSION ((major), (minor), (micro)) || \
     gtk_check_version ((major), (minor), (micro)))
# define GTK_HAVE(func,major,minor,micro) \
    (GTK_CHECK_VERSION ((major), (minor), (micro)) || (func))
#endif

#define GXINE_GTK_OLDER_THAN(major,minor,micro) \
    (defined GXINE_GTK_COMPAT || !GTK_CHECK_VERSION ((major), (minor), (micro)))

#define WEAK(type,func) extern type func () __attribute__ ((weak))

/* debug logging */
#ifdef LOG
#define logprintf(FMT,...) g_print (FMT, ## __VA_ARGS__)
#define logperror(STR) perror (STR)
#else
#define logprintf(FMT,...)  do{}while(0)
#define logperror(STR)      do{}while(0)
#endif

#define GXINE_TRUE (xine_config_cb_t)gtk_true

#endif
