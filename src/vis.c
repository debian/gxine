/*
 * Copyright (C) 2004-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * Visualisation wrapper code
 */

#include "globals.h"

#include <string.h>

#include "vis.h"

#include "engine.h"
#include "script_engine.h"
#include "ui.h"
#include "utils.h"

gboolean vis_show (GtkVideo *this, xine_audio_port_t **audio_port)
{
  if (xine_get_stream_info (stream, XINE_STREAM_INFO_HAS_VIDEO) ||
      !xine_get_stream_info (stream, XINE_STREAM_INFO_HAS_AUDIO))
    return FALSE;
  return gtk_video_set_vis (this, audio_port, TRUE);
}

void vis_cb (GtkRadioAction *action, gpointer data)
{
  const char *const *pol =
    xine_list_post_plugins_typed (xine, XINE_POST_TYPE_AUDIO_VISUALIZATION);
  if (pol)
  {
    int i = gtk_radio_action_get_current_value (action);
    gtk_video_select_vis ((GtkVideo *)gtv, i ? pol[i - 1] : NULL, &audio_port);
    window_check_vis (TRUE);
  }
}

void vis_set (const char *str)
{
  GSList *action;
  foreach_glist (action, action_items.vis)
  {
    char *name;
    g_object_get (action->data, "name", &name, NULL);
    if (name[0] != '_' && !strcasecmp (str, "none"))
      break;
    g_object_get (action->data, "label", &name, NULL);
    if (!strcasecmp (str, name))
      break;
  }
  if (action)
    gtk_action_activate (action->data);
}

static JSBool js_set_vis (JSContext *cx, uintN argc, jsval *vp)
{
  jsval *argv = JS_ARGV (cx, vp);
  se_log_fncall ("js_set_vis");
  se_argc_check (1, "set_vis");
  se_arg_is_string (0, "set_vis");

  JSString *str = JS_ValueToString (cx, argv[0]);
  char *cstr = SE_JS_ENCODE_STRING (cx, str);
  vis_set (cstr);
  SE_JS_FREE_ENCODED_STRING (cx, cstr);

  JS_SET_RVAL (cx, vp, JSVAL_VOID);
  return JS_TRUE;
}

void vis_init (void)
{
  xine_cfg_entry_t entry;
  se_defun (gse, NULL, "set_vis", js_set_vis, 0, JSFUN_FAST_NATIVE,
	    SE_GROUP_ENGINE, N_("string"), N_("visualisation name"));
  if (xine_config_lookup_entry (xine, "post_audio_plugin", &entry))
    gtk_video_select_vis ((GtkVideo *)gtv, entry.str_value, &audio_port);
}
