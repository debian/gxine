/*
 * Copyright (C) 2003-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * open mrl dialog (plus file browser)
 */

#include "globals.h"

#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <gtk/gtk.h>

#include "open_mrl.h"
#include "playlist.h"
#include "ui.h"
#include "utils.h"
#include "script_engine.h"
#include "history.h"

static GtkWidget      *mrl_dialog;
static gboolean        mrl_visible;
static GtkWidget      *mrl_entry;

void file_dialog_show(void)
{
  playlist_add_list
    (modal_multi_file_dialog (_("Select files to add to playlist"),
			      FALSE, NULL, app),
     TRUE);
}

void file_dialog_init (void)
{
  /* nothing to do */
}

static gboolean responding = FALSE;

static void
open_mrl_activate_cb (GtkWidget *w, char *cmrl)
{
  while (isspace (*cmrl))
    ++cmrl;
  if (*cmrl)
  {
    int item = playlist_add_mrl (cmrl, -1);
    playlist_play (item);
  }
  if (!responding)
  {
    mrl_visible = FALSE;
    gtk_widget_hide (mrl_dialog);
  }
}

static void open_mrl_response_cb (GtkDialog *dbox, int response, gpointer data)
{
  switch (response)
  {
  case 1:
    get_mrl_from_filesystem ((GtkWindow *)mrl_dialog,
			     (GtkEntry *)history_combo_box_get_entry (mrl_entry));
    break;

  case GTK_RESPONSE_OK:
    responding = TRUE;
    history_combo_box_activate (mrl_entry);
    responding = FALSE;

  default:
    mrl_visible = FALSE;
    gtk_widget_hide (mrl_dialog);
  }
}

static JSBool js_open_show (JSContext *cx, uintN argc, jsval *vp)
{
  /* se_t *se = (se_t *) JS_GetContextPrivate(cx); */
  se_log_fncall_checkinit ("open_show");
  file_dialog_show ();
  JS_SET_RVAL (cx, vp, JSVAL_VOID);
  return JS_TRUE;
}

static JSBool js_open_mrl_show (JSContext *cx, uintN argc, jsval *vp)
{
  /* se_t *se = (se_t *) JS_GetContextPrivate(cx); */
  se_log_fncall_checkinit ("open_mrl_show");
  open_mrl_show ();
  JS_SET_RVAL (cx, vp, JSVAL_VOID);
  return JS_TRUE;
}

void open_mrl_init (void)
{
  GtkWidget *hbox;

  mrl_dialog = gtk_dialog_new_with_buttons (_("Open MRL..."), NULL, 0, NULL);
  gtk_dialog_add_action_widget (GTK_DIALOG(mrl_dialog),
				ui_button_new_stock_mnemonic
				  (GTK_STOCK_OPEN, _("_File...")),
				1);
  gtk_dialog_add_buttons (GTK_DIALOG(mrl_dialog),
			  GTK_STOCK_CANCEL, GTK_RESPONSE_DELETE_EVENT,
			  GTK_STOCK_OK, GTK_RESPONSE_OK,
			  NULL);

  gtk_window_set_default_size (GTK_WINDOW (mrl_dialog), 400, -1);
  gtk_window_set_position (GTK_WINDOW (mrl_dialog), GTK_WIN_POS_CENTER);
  hide_on_delete (mrl_dialog, &mrl_visible);
  g_signal_connect (G_OBJECT (mrl_dialog), "response",
		    G_CALLBACK (open_mrl_response_cb), NULL);
  gtk_dialog_set_default_response (GTK_DIALOG(mrl_dialog), GTK_RESPONSE_OK);

  hbox = gtk_hbox_new (0, 2);
  gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (mrl_dialog))),
                      hbox, TRUE, TRUE, 0);

  mrl_entry = history_combo_box_new (open_mrl_activate_cb);
  gtk_box_pack_start (GTK_BOX(hbox), mrl_entry, TRUE, TRUE, 2);

  GtkWidget *b = ui_button_new_stock_mnemonic (GTK_STOCK_CLEAR, _("_Flush"));
  gtk_box_pack_start (GTK_BOX (hbox), b, FALSE, FALSE, 0);
  g_signal_connect_swapped (b, "clicked",
			    G_CALLBACK (history_combo_box_clear), mrl_entry);

  mrl_visible = 0;

  {
    static const se_f_def_t defs[] = {
      { "open_show", js_open_show, 0, JSFUN_FAST_NATIVE, SE_GROUP_FILE, NULL, NULL },
      { "open_mrl_show", js_open_mrl_show, 0, JSFUN_FAST_NATIVE, SE_GROUP_FILE, NULL, NULL },
      { NULL }
    };
    se_defuns (gse, NULL, defs);
  }
}

void open_mrl_show (void)
{
  if (mrl_visible)
  {
    mrl_visible = FALSE;
    gtk_widget_hide (mrl_dialog);
  }
  else
  {
    mrl_visible = TRUE;
    window_show (mrl_dialog, NULL);
    gtk_widget_grab_focus (mrl_entry);
  }
}
