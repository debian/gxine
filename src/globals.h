/*
 * Copyright (C) 2001-2017 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * global variables for lazy programmers... :>
 */

#ifndef GXINE_GLOBALS_H
#define GXINE_GLOBALS_H

/*
#define EXP_STUFF
#define LOCK_DEBUG
*/

#include "config.h"
#include "i18n.h"

#include <pthread.h>

#include <gtk/gtk.h>
#include <glib.h>

#include <xine.h>

#include "script_engine.h"
#include "info_widgets.h"

#include "defs.h"

typedef enum {
  CONFIG_VERSION_BASE,
  CONFIG_VERSION_0_6_0,
  CONFIG_VERSION_CURRENT = CONFIG_VERSION_0_6_0
} config_version_t;

extern gboolean have_config;
extern int config_version;
#define CONFIG_VERSION_ATLEAST(EPOCH, MAJOR, MINOR) \
  (!have_config || config_version >= CONFIG_VERSION_##EPOCH##_##MAJOR##_##MINOR)

extern GtkWidget      *app;
extern GtkWidget      *gtv; /* GtkVideo widget */
extern GtkWidget      *menubar, *popup_menu;
extern GtkWidget      *keypad;
extern GSList	      *infobars, *timewidgets;
extern pthread_mutex_t widgets_update_lock;
extern gboolean	       initialised;
extern GRecMutex engine_lock;

typedef struct {
  GtkToggleAction *fullscreen, *resize_factor, *auto_resize, *auto_rescale,
		  *deinterlace, *vo_postproc, *ao_postproc, *fs_toolbar,
		  *wm_toolbar, *wm_toolbar_snap, *wm_unblank, *fs_sticky;
  GSList *aspect, *vis, *fs_toolbar_pos, *video_size, *subtitles;
  GtkAction *keypad, *hidemain, *showmain;
} action_items_t;
extern action_items_t  action_items;

extern int             verbosity;
extern xine_t         *xine;
extern xine_stream_t  *stream;
extern se_t           *gse; /* global script engine */
extern const char     *plugindir, *bindir, *logodir, *pixmapdir, *icondir, *miscdir, *confdir;
extern xine_audio_port_t *audio_port;
extern xine_video_port_t *video_port;

extern GAsyncQueue    *js_queue;

extern gboolean        fs_always_sticky, fs_is_sticky;

#define MAX_MRL_LEN 1024

#define CONFIGDIR		"."PACKAGE
#define FILE_ACCELS		"accels.scm"
#define FILE_CONFIG		"config"
#define FILE_GTKRC		"gtkrc"
#define FILE_GTKCSS             "gtk.css"
#define FILE_KEYBINDINGS	"keybindings"
#define FILE_MEDIAMARKS		"mediamarks"
#define FILE_PLAYLIST		"playlist"
#define FILE_STARTUP		"startup"

/* used in key event handlers for filtering out numlock etc. */
#define GXINE_MODIFIER_MASK \
  (GDK_SHIFT_MASK | GDK_CONTROL_MASK | \
   GDK_MOD1_MASK | GDK_MOD3_MASK | GDK_MOD4_MASK)

#define CONFIG_DATA_NONE ((void *)1)

#ifdef LOCK_DEBUG
#define pthread_mutex_lock(M) gxine_mutex_lock ((M), __FUNCTION__, __LINE__)
#define pthread_mutex_unlock(M) gxine_mutex_unlock ((M), __FUNCTION__, __LINE__)
#define gdk_threads_enter() gxine_threads_enter (__FUNCTION__, __LINE__)
#define gdk_threads_leave() gxine_threads_leave (__FUNCTION__, __LINE__)
extern int gxine_mutex_lock (pthread_mutex_t *, const char *, int);
extern int gxine_mutex_unlock (pthread_mutex_t *, const char *, int);
extern void gxine_threads_enter (const char *, int);
extern void gxine_threads_leave (const char *, int);
#endif

#ifdef HAVE_XML_PARSER_REENTRANT
# define xml_parser_init_R(X,D,L,M) X = xml_parser_init_r ((D), (L), (M))
# define xml_parser_build_tree_R(X,T) xml_parser_build_tree_r ((X), (T))
# define xml_parser_finalize_R(X) xml_parser_finalize_r ((X))
#else
# define xml_parser_init_R(X,D,L,M) xml_parser_init ((D), (L), (M))
# define xml_parser_build_tree_R(X,T) xml_parser_build_tree ((T))
# define xml_parser_finalize_R(X) do {} while (0)
#endif

#endif
