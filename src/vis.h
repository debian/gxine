/*
 * Copyright (C) 2004-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * Visualisation wrapper code
 */

#ifndef GXINE_VIS_H
#define GXINE_VIS_H

#include <gtk/gtk.h>

#include "gtkvideo.h"

#define vis_hide(V,A) gtk_video_set_vis ((V), (A), FALSE)
gboolean vis_show (GtkVideo *, xine_audio_port_t **);
void vis_set (const char *);
void vis_cb (GtkRadioAction *, gpointer);

void vis_init (void);

#endif
