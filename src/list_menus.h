/*
 * Copyright (C) 2007 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

/* Media marks & playlist menu popups (common code) */

#ifndef _GXINE_LIST_MENUS_H_
#define _GXINE_LIST_MENUS_H_

#include <gtk/gtk.h>

gboolean list_menu_click_cb (GtkTreeView *, GdkEventButton *,
			     GtkMenu *const *);
gboolean list_menu_popup_cb (GtkWidget *, GdkEventButton *, GtkMenu *);
gboolean list_menu_key_press_cb (GtkTreeView *, GdkEventKey *,
				 GtkMenu *const *);

#endif
