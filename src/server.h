/*
 * Copyright (C) 2002 Stefan Holst
 * Copyright (C) 2003-2006 the xine project team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * gxine remote control via socket
 */

#ifndef GXINE_SERVER_H
#define GXINE_SERVER_H

/* functions to attempt to connect to already running gxine */
int  server_client_connect (void);
int  server_client_send (const char *cmd);
void server_client_stop (void);

/* if no gxine is running, create our own gxine socket: */
void server_setup (void);
void server_start (void);
void server_stop  (void);

#endif
