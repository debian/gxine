/*
 * Copyright (C) 2001-2006 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * helper functions to register gxine with kde and gnome
 * (and possibly cde in the future)
 */

#ifndef GXINE_DESKTOP_INTEGRATION
#define GXINE_DESKTOP_INTEGRATION

gboolean gxine_vfs_init (void);

#ifdef USE_INTEGRATION_WIZARD
const char *di_registration_report (void);
void di_registration_flush (void);
void di_register_gnome (void);
void di_register_kde (void);
void di_register_mailcap (void);
void di_register_mozilla (void);
#endif

#endif
