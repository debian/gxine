/*
 * Copyright (C) 2001-2006 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * key event handler, keymap editor
 */

#ifndef GXINE_KEY_EVENTS_H
#define GXINE_KEY_EVENTS_H

#include <gtk/gtk.h>

struct kb_xine_event_id_s
{
  char name[16];
  int number;
};
typedef struct kb_xine_event_id_s kb_xine_event_id_t;

struct kb_xine_event_map_s {
  char prefix[8];
  const kb_xine_event_id_t id[];
};
typedef struct kb_xine_event_map_s kb_xine_event_map_t;

void get_menu_bar_accel (GtkWidget *, guint *, GdkModifierType *);

gboolean keypress_cb (GtkWidget *, GdkEventKey *, gpointer);
gboolean buttonpress_cb (GtkWidget *, GdkEventButton *, gpointer);
gboolean buttonrelease_cb (GtkWidget *, GdkEventButton *, gpointer);

void kb_edit_show (void);

void key_events_init (void);

void save_key_bindings (void);

/* returns xine event number (XINE_EVENT_*) or 0 on failure */
int kb_xine_event_lookup (const kb_xine_event_map_t *, const char *);

JSBool js_event_generic (JSContext *, uintN argc, jsval *vp, const char *func,
			 const char *const *mrlprefix, ...)
			 __attribute__ ((sentinel));

#endif
