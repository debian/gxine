/*
 * Copyright (C) 2004-2017 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * xine engine wrapper
 */
#ifndef GXINE_PLAYER_H
#define GXINE_PLAYER_H

void player_init (const char *audio_driver_id);

void play_exec_error_suppress_next (void);

const char *player_get_cur_title (void);
const char *player_get_cur_mrl (void);

void player_launch (const char *title, const char *mrl,
		    int pos, int pos_time, int duration);
void player_wait (void);
void player_stop (void);

int player_live_stream (void);

#endif
