/*
 * Copyright (C) 2002-2006 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * playlist for gxine
 */
#ifndef GXINE_PLAYLIST_H
#define GXINE_PLAYLIST_H

#include "play_item.h"

void playlist_init (void);

void playlist_show (void);

void playlist_clear (void);
int playlist_flush (play_item_type);

int  playlist_add     (play_item_t *play_item, gint ins_pos /* -1 => append */);
int  playlist_add_mrl (const char *mrl, gint ins_pos /* -1 => append */);
void playlist_add_list (GSList *, gboolean play); /* also frees the list */

void playlist_check_set_title (void);

void playlist_play      (int list_pos);
void playlist_play_from (int list_pos, int pos, int pos_time);

void playlist_logo (gpointer wait_if_non_null);

int playlist_showing_logo (void);

play_item_t *playlist_get_item     (int list_pos); /* pointer; NOT a copy! */
int          playlist_get_list_pos (void); /*of currently played entry*/
play_item_t *playlist_get_current_item (void); /* copy of current; unedited */

int playlist_size (void);

void playlist_save (char *filename);

#endif
