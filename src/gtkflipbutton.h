/*
 * Copyright (C) 2004-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * a GTK toggle-button-like widget
 */

#ifndef GXINE_GTK_FLIP_BUTTON_H
#define GXINE_GTK_FLIP_BUTTON_H

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

/* Signals */
  enum
  {
    GTK_FLIP_BUTTON_TOGGLED,
    GTK_FLIP_BUTTON_LAST_SIGNAL,
  };

#define GTK_FLIP_BUTTON(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), gtk_flip_button_get_type (), GtkFlipButton))
#define GTK_FLIP_BUTTON_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), gtk_flip_button_get_type (), GtkFlipButtonClass))
#define GTK_IS_FLIP_BUTTON(obj)		(G_TYPE_CHECK_INSTANCE_TYPE (obj, gtk_flip_button_get_type ()))
#define GTK_IS_FLIP_BUTTON_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), gtk_flip_button_get_type ()))

  typedef struct _GtkFlipButton GtkFlipButton;
  typedef struct _GtkFlipButtonClass GtkFlipButtonClass;
  typedef struct gtk_flip_button_private_s gtk_flip_button_private_t;

  struct _GtkFlipButton
  {
    GtkToggleButton button;
    gtk_flip_button_private_t *priv;	/* private data */
  };

  struct _GtkFlipButtonClass
  {
    GtkToggleButtonClass parent_class;
    void (*toggled) (GtkWidget *gfb);
  };

  GType gtk_flip_button_get_type (void);
  GtkWidget *gtk_flip_button_new (GtkWidget *, GtkWidget *);
  GtkWidget *gtk_flip_button_new_from_stock (const char *, const char *,
					     GtkIconSize);
  GtkWidget *gtk_flip_button_new_from_icon_names (const char *, const char *,
						  GtkIconSize);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* GXINE_GTK_FLIP_BUTTON_H */
