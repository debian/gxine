/*
 * Copyright (C) 2017 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

/* Media marks & playlist menu popups (common code) */

#include "globals.h"

#include <gdk/gdkkeysyms.h>
#if GTK_CHECK_VERSION(3, 0, 0)
#include <gdk/gdkkeysyms-compat.h>
#endif

#include "key_events.h"
#include "list_menus.h"

gboolean list_menu_click_cb (GtkTreeView *tree, GdkEventButton *ev,
			     GtkMenu *const *menus)
{
  if (ev->button != 3 || ev->window != gtk_tree_view_get_bin_window (tree))
    return FALSE;

  GtkTreePath *path;
  if (gtk_tree_view_get_path_at_pos (tree, ev->x, ev->y, &path,
				     NULL, NULL, NULL))
  {
    gtk_tree_selection_select_path (gtk_tree_view_get_selection (tree), path);
    gtk_tree_path_free (path);
    gtk_menu_popup (menus[1], NULL, NULL, NULL, NULL, 3,
		    gtk_get_current_event_time ());
  }
  else
    gtk_menu_popup (menus[0], NULL, NULL, NULL, NULL, 3,
		    gtk_get_current_event_time ());

  return TRUE;
}

gboolean list_menu_popup_cb (GtkWidget *widget, GdkEventButton *ev,
			     GtkMenu *data)
{
  if (ev->button != 3)
    return FALSE;
  gtk_menu_popup (data, NULL, NULL, NULL, NULL, 3, gtk_get_current_event_time ());
  return TRUE;
}

gboolean list_menu_key_press_cb (GtkTreeView *tree, GdkEventKey *ev,
				 GtkMenu *const *menus)
{
  guint keyval = 0;
  GdkModifierType mods;
  get_menu_bar_accel ((GtkWidget *)tree, &keyval, &mods);

  if ((ev->keyval != keyval || ev->state == mods) &&
      (ev->keyval != GDK_Menu || ev->state != 0))
    return FALSE;

  GtkTreeIter iter;
  GtkTreeSelection *sel = gtk_tree_view_get_selection (tree);
  int which = gtk_tree_selection_get_selected (sel, NULL, &iter);
  gtk_menu_popup (menus[which], NULL, NULL, NULL, NULL, 0,
		  gtk_get_current_event_time ());
  return TRUE;
}
