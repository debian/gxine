/*
 * Copyright (C) 2004-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * GTK flip-button widget
 */

#include "config.h"
#include "i18n.h"

#include "defs.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>

#include <gtk/gtk.h>

#include "gtkflipbutton.h"

/* private data */

struct gtk_flip_button_private_s
{
  GtkWidget *inactive, *active;
};

GType
gtk_flip_button_get_type (void)
{
  static GType gtk_flip_button_type = 0;

  if (!gtk_flip_button_type)
  {
    static const GTypeInfo gtk_flip_button_info = {
      .class_size = sizeof (GtkFlipButtonClass),
      .instance_size = sizeof (GtkFlipButton),
    };

    gtk_flip_button_type =
      g_type_register_static (GTK_TYPE_TOGGLE_BUTTON, "GtkFlipButton",
			      &gtk_flip_button_info, (GTypeFlags) 0);
  }

  return gtk_flip_button_type;
}

static gboolean gfb_toggled (GtkFlipButton *this)
{
  if (gtk_widget_get_realized((GtkWidget *)this))
  {
    gboolean active = gtk_toggle_button_get_active (&this->button);
    gtk_widget_unmap (active ? this->priv->inactive
                             : this->priv->active);
    gtk_widget_map (active ? this->priv->active
                           : this->priv->inactive);
  }
  return FALSE;
}

GtkWidget *
gtk_flip_button_new (GtkWidget *inactive, GtkWidget *active)
{
  GtkWidget *this = g_object_new (gtk_flip_button_get_type (), NULL);
  GtkWidget *table;

  ((GtkFlipButton *) this)->priv = malloc (sizeof (gtk_flip_button_private_t));
  ((GtkFlipButton *) this)->priv->inactive = inactive;
  ((GtkFlipButton *) this)->priv->active = active;

  table = gtk_table_new (2, 1, FALSE);
  gtk_table_attach_defaults ((GtkTable *)table, inactive, 0, 1, 0, 1);
  gtk_table_attach_defaults ((GtkTable *)table, active, 0, 1, 0, 1);
  gtk_container_add (GTK_CONTAINER(this), table);
  gtk_widget_show_all (table);
  gtk_widget_unmap (active);

  g_object_connect (this,
	"signal-after::toggled", G_CALLBACK(gfb_toggled), NULL,
	"signal-after::map-event", G_CALLBACK(gfb_toggled), NULL,
	"signal-after::show", G_CALLBACK(gfb_toggled), NULL,
	NULL);

  return this;
}

GtkWidget *
gtk_flip_button_new_from_stock (const char *inactive, const char *active,
				GtkIconSize size)
{
  GtkWidget *wi = gtk_image_new_from_stock (inactive, size);
  GtkWidget *wa = gtk_image_new_from_stock (active, size);
  return gtk_flip_button_new (wi, wa);
}

GtkWidget *
gtk_flip_button_new_from_icon_names (const char *inactive, const char *active,
				     GtkIconSize size)
{
  GtkWidget *wi = gtk_image_new_from_icon_name (inactive, size);
  GtkWidget *wa = gtk_image_new_from_icon_name (active, size);
  return gtk_flip_button_new (wi, wa);
}

