/*
 * Copyright (C) 2003-2017 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * standard, non-skinned main window
 */

#ifndef GXINE_NOSKIN_WINDOW_H
#define GXINE_NOSKIN_WINDOW_H

#include <glib.h>

void noskin_main_init (const char *video_driver_id, const gchar *geometry, gboolean fullscreen);
void noskin_main_close (void);

void noskin_post_init (void);
void noskin_main_post_init (gint fullscreen);

void app_show (void);
void app_hide (void);

#endif
