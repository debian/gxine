/*
 * Copyright (C) 2003-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * main/context menu creation / handling
 */

#ifndef GXINE_MENU_H
#define GXINE_MENU_H

#include <gtk/gtk.h>

void create_menus (GtkWidget *window);
void menu_add_items (GtkUIManager *, const char *const prefix,
		     const char *const label, guint merge, size_t count);
const char *menu_get_tree_name (unsigned int);

/* signal callback & support for dynamic scale changes */
void scale_changed_cb (GtkWidget *, gdouble, gpointer);
GtkAction *find_video_size_action (gdouble);

void update_subtitles_menu (void);

extern GtkUIManager *ui;

#endif
