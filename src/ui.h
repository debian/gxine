/*
 * Copyright (C) 2001-2017 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * needful things
 */

#ifndef GXINE_UI_H
#define GXINE_UI_H

#include <gtk/gtk.h>
#include <xine.h>
#include "script_engine.h"

#define GXINE_MEDIA_SPEAKER	 "gxine-media-speaker"
#define GXINE_MEDIA_SPEAKER_MUTE "gxine-media-speaker-mute"
#define GXINE_LOGO		 "gxine"
#define GXINE_MEDIA_MARK	 "gxine-media-mark"

extern GtkIconSize icon_size_logo;

/* UI status attributes */
typedef enum {
  UI_CURRENT_STATE = -1,
  /* Player state */
  UI_STOP = 0,
  UI_PAUSE,
  UI_PLAY_SLOW,
  UI_PLAY,
  UI_FAST_FORWARD,
  /* Mute */
  UI_AUDIO_UNMUTE, UI_AUDIO_MUTE,
  /* Full-screen toolbar */
  UI_FS_TOOLBAR,
  UI_FS_TOOLBAR_POS,
  UI_WM_TOOLBAR,
  UI_WM_TOOLBAR_SNAP,
} ui_status_t;

/* Set/show play/pause/stop/mute status in control buttons */
void ui_set_status (ui_status_t);

/* UI button classes */
typedef enum {
  Control_PLAY,
  Control_REWIND,
  Control_FASTFWD,
  Control_PAUSE,
  Control_STOP,
  Control_MUTE,
  Control_Buttons,
  Control_PlayerButtons = Control_STOP + 1 /* MUTE is special-cased */
} control_button_t;

/* UI slider/spinner classes (adjustment types) */
typedef enum {
  Control_SEEKER,
  Control_AUDIO_CHANNEL,
  Control_SPU_CHANNEL,
  Control_VOLUME,
  Control_COMPRESSOR,
  Control_AMPLIFIER,
  Control_AV_SYNC,
  Control_SPU_SYNC,
  Control_HUE,
  Control_SATURATION,
  Control_CONTRAST,
  Control_BRIGHTNESS,
#ifdef XINE_PARAM_VO_GAMMA
  Control_GAMMA,
#endif
#ifdef XINE_PARAM_VO_SHARPNESS
  Control_SHARPNESS,
  Control_NOISE_REDUCE,
#endif
  Control_EQ_30,
  Control_EQ_60,
  Control_EQ_125,
  Control_EQ_250,
  Control_EQ_500,
  Control_EQ_1K,
  Control_EQ_2K,
  Control_EQ_4K,
  Control_EQ_8K,
  Control_EQ_16K,
  Control_Adjustments
} control_adjustment_t;

/* Register a button for the above status functions */
void ui_register_control_button (control_button_t, GtkWidget *);
/* Get the adjustment object for a given adjustment type */
GtkAdjustment *ui_register_control_adjustment (control_adjustment_t);

/* Callback functions for registered buttons and our adjustments must check
 * this and return without effect if this is TRUE.
 */
extern gboolean no_recursion;

extern gboolean fs_toolbar_visible, fs_toolbar_at_top,
		wm_toolbar_visible, wm_toolbar_snap;

void ui_toolbar_toggle (void);

/* Set a button's state or adjustment's value */
void ui_set_control_button (control_button_t, gboolean);
void ui_set_control_adjustment (control_adjustment_t, gdouble);
void ui_reset_control_adjustment (control_adjustment_t);
void ui_revert_control_adjustment (control_adjustment_t);
void ui_clear_control_adjustment (control_adjustment_t);
/* Update objects which use the given adjustment */
void ui_xine_set_param_from_adjustment (control_adjustment_t);

void ui_register_adjustment_widget (control_adjustment_t, GtkWidget *);

void ui_preferences_register (xine_t *);

/* Property creation */
enum ui_property_type_t {
  UI_PROP_BOOL,
  UI_PROP_XINE_PARAM,
  UI_PROP_GTK_WIDGET,
  UI_PROP_GTK_VIDEO,
  UI_PROP_CUSTOM,
};
#define UI_BOOL(ACTION, VAR, SET) { .flag = { SET, &VAR, &action_items.ACTION } }, UI_PROP_BOOL
#define UI_XINE_PARAM(PARAM, SET) { .xine = { SET, PARAM } }, UI_PROP_XINE_PARAM
#define UI_GTK_WIDGET(WIDGET, GET, SET) { .widget = { SET, GET, &WIDGET } }, UI_PROP_GTK_WIDGET
#define UI_GTK_VIDEO(ACTION, GET) { .video = { NULL, GET, &action_items.ACTION } }, UI_PROP_GTK_VIDEO
#define UI_CUSTOM(GET, SET) { .custom = { SET, GET } }, UI_PROP_CUSTOM

struct _GtkVideo;

typedef struct ui_property_s {
  const char *name, *help;
  union {
    struct {
      void (*set) (int);
      gboolean *flag;
      GtkToggleAction **action;
    } flag;
    struct {
      void (*set) (int);
      int param;
    } xine;
    struct {
      void (*set) (GtkToggleButton *, gint);
      int (*get) (GtkToggleButton *);
      GtkToggleButton **widget;
    } widget;
    struct {
      void (*set) (); /* unused */
      int (*get) (struct _GtkVideo *);
      GtkToggleAction **action;
    } video;
    struct {
      void (*set) (int);
      void (*get) (const struct ui_property_s *, se_prop_read_t *);
    } custom;
  } ui;
  enum ui_property_type_t type;
  int min, max;
  gboolean loop;
  se_prop_listener_t listen;
} ui_property_t;

int ui_property_clip_int (const ui_property_t *, int);
void ui_create_properties (const ui_property_t *, se_o_t *parent,
			   se_prop_type_t);

/* Button creation */
GtkWidget *ui_button_new_stock (const char *);
GtkWidget *ui_toggle_button_new_stock (const char *);
GtkWidget *ui_button_new_stock_mnemonic (const char *stock,
					 const char *mnemonic);
GtkWidget *ui_button_new_icon_name (const char *);
GtkWidget *ui_toggle_button_new_icon_name (const char *);
GtkWidget *ui_button_new_icon_name_mnemonic (const char *name,
					     const char *mnemonic);

/* Misc creation */
GtkWidget *ui_spin_button_new (GtkAdjustment *adj);
GtkWidget *ui_hscale_new (GtkAdjustment *adj, GtkPositionType, int digits);
GtkWidget *ui_label_new_with_xalign (const char *, gfloat);
GtkWidget *ui_label_new_with_markup (const char *);

/* Menu/toolbar basics */
GtkUIManager *ui_create_manager (const char *label, GtkWidget *window);
#define ui_get_action_group(ui) \
  ((GtkActionGroup *)(gtk_ui_manager_get_action_groups ((ui))->data))
void ui_mark_active (GtkUIManager *, const char *const items[], gboolean);
void gtk_action_group_connect_accelerators (GtkActionGroup *);

GtkAccelGroup *ui_add_undo_response (GtkWidget *window, GtkAccelGroup *);

/* In noskin_window.c */
void window_fs_toolbar_show (gboolean);
void window_fs_toolbar_position (gboolean top);
void window_fs_toolbar_reset (void);
void window_fs_toolbar_restore (void);
void window_wm_toolbar_show (gboolean);
void window_wm_toolbar_reset (void);
void window_wm_toolbar_restore (void);
void window_wm_toolbar_set_snap (gboolean);
gboolean window_wm_toolbar_snap (void);
void window_check_vis (gboolean force);
void window_fs_stickiness_update (gboolean);

void ui_init (void);
void ui_post_init (void);

#endif
