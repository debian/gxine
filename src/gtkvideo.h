/*
 * Copyright (C) 2001-2017 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * a gtk xine video widget
 */

#ifndef GXINE_GTK_VIDEO_H
#define GXINE_GTK_VIDEO_H

#include <gtk/gtk.h>
#include <xine.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Signals */
enum {
  GTK_VIDEO_SCALE_FACTOR, /* video scale has been changed by a resize */
  LAST_SIGNAL
};


#define GTK_VIDEO_MIN_WIDTH 32
#define GTK_VIDEO_MIN_HEIGHT 24

#define GTK_VIDEO(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), gtk_video_get_type (), GtkVideo))
#define GTK_VIDEO_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), gtk_video_get_type (), GtkVideoClass))
#define GTK_IS_VIDEO(obj)          (G_TYPE_CHECK_INSTANCE_TYPE (obj, gtk_video_get_type ()))
#define GTK_IS_VIDEO_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), gtk_video_get_type ()))

  typedef struct _GtkVideo      GtkVideo;
  typedef struct _GtkVideoClass GtkVideoClass;

  typedef void (*gtk_video_frame_cb_t) (gpointer);

  typedef struct gtk_video_private_s gtk_video_private_t;


  struct _GtkVideo
  {
    GtkWidget                widget;
    gtk_video_private_t     *priv; /* private data */
  };

  struct _GtkVideoClass
  {
    GtkWidgetClass parent_class;
    void (*keypress) (GtkWidget *, GdkEventKey *, gpointer);
    void (*keyrelease) (GtkWidget *, GdkEventKey *, gpointer);
    void (*scale_factor) (GtkWidget *, double, gpointer);
  };

  GType      gtk_video_get_type          (void);
  GtkWidget* gtk_video_new               (xine_t *xine, xine_stream_t *stream,
					  xine_post_out_t *post_out, /* will be rewired */
                                          const char *video_driver_id,
					  int default_width, int default_height,
					  int button_press_mask,
					  int button_release_mask);
  void	     gtk_video_set_frame_cb	 (GtkVideo *, gtk_video_frame_cb_t,
					  gpointer);
  void	     gtk_video_set_end_time	 (GtkVideo *, gint);
  void       gtk_video_set_visibility    (GtkVideo *gtv,
					  GdkVisibilityState state);
  void       gtk_video_resize            (GtkVideo *gtv,
					  gint x, gint y,
					  gint width,
					  gint height);
  void       gtk_video_rescale           (GtkVideo *gtv, double scale);
  xine_video_port_t *gtk_video_get_port  (GtkVideo *gtv);
  void gtk_video_reshow (GtkVideo *);
  gboolean gtk_video_is_fullscreen (GtkVideo *gtv);
  void gtk_video_set_fullscreen (GtkVideo *, gboolean, GRecMutex *const);
    /* note: up to the caller to make sticky etc.; use window-state-event */
  void gtk_video_make_transient_for_fullscreen (GtkVideo *gtv, GtkWindow *);
  const GtkRequisition *gtk_video_get_fullscreen_geometry (GtkVideo *gtv);
  gboolean gtk_video_get_auto_resize (GtkVideo *gtv);
  void gtk_video_set_auto_resize (GtkVideo *gtv, gboolean resize);
  gboolean gtk_video_get_windowed_unblank (GtkVideo *gtv);
  void gtk_video_set_windowed_unblank (GtkVideo *gtv, gboolean unblank);
  void gtk_video_deny_shrink (GtkVideo *gtv);
  gboolean gtk_video_allow_shrink (GtkVideo *gtv);

  gboolean gtk_video_select_vis (GtkVideo *, const char *id,
				 xine_audio_port_t **);
  gboolean gtk_video_set_vis (GtkVideo *, xine_audio_port_t **, gboolean);

  void gtk_video_set_post_plugins_deinterlace (GtkVideo *, const char *);
  void gtk_video_set_post_plugins_video (GtkVideo *, const char *);
  void gtk_video_set_post_plugins_audio (GtkVideo *, const char *,
					 xine_audio_port_t *);
  void gtk_video_set_use_post_plugins_deinterlace (GtkVideo *, gboolean);
  void gtk_video_set_use_post_plugins_video (GtkVideo *, gboolean);
  void gtk_video_set_use_post_plugins_audio (GtkVideo *, gboolean,
					     xine_audio_port_t *);
  gboolean gtk_video_get_use_post_plugins_deinterlace (GtkVideo *);
  gboolean gtk_video_get_use_post_plugins_video (GtkVideo *);
  gboolean gtk_video_get_use_post_plugins_audio (GtkVideo *);

  void gtk_video_set_auto_rescale (GtkVideo *, gboolean);
  gboolean gtk_video_get_auto_rescale (GtkVideo *);

  void gtk_video_unblank_screen (GtkVideo *);

  uint32_t gtk_video_get_capabilities (GtkVideo *); /* video port caps */

  gboolean gtk_video_in_spu_button (GtkVideo *, int);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GXINE_GTK_VIDEO_H */
