/*
 * Copyright (C) 2001-2017 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * needful things
 */

#ifndef GXINE_UTILS_H
#define GXINE_UTILS_H

#include <gtk/gtk.h>
#include <glib.h>

void utils_init (void);

int ensure_path_exists (char *path, int mode);

char *make_path_uri (const char *);

/* read an entire file and ensure that the buffer is LF+NUL-terminated
 * file_size may be NULL
 */
char *read_entire_file (const char *mrl, ssize_t *file_size);

char *get_config_filename (const char *leaf);

/* open or close a file, checking for errors */
FILE *open_write (const char *name, const char *errtitle);
void close_write (const char *name, FILE *fd, const char *errtitle);

/* display message boxes
 * if fmt==NULL, first vararg is format string and window is modal
 */
typedef enum {
  FROM_GXINE, FROM_XINE, FROM_XINE_COMBINABLE
} gxine_msg_source;

GtkWidget *v_display_message (gxine_msg_source, gboolean modal, const gchar *primary,
			      GtkMessageType, const gchar *fmt, va_list)
			      __attribute__ ((format (printf, 5, 0)));
void display_error_modal (gxine_msg_source, const gchar *, const gchar *, ...)
	__attribute__ ((format (printf, 3, 4)));
void display_error (gxine_msg_source, const gchar *, const gchar *, ...)
	__attribute__ ((format (printf, 3, 4)));
void display_warning (gxine_msg_source, const gchar *, const gchar *, ...)
	__attribute__ ((format (printf, 3, 4)));
void display_info (gxine_msg_source, const gchar *, const gchar *, ...)
	__attribute__ ((format (printf, 3, 4)));

void display_error_combinable (gxine_msg_source, const gchar *, const gchar *, ...)
	__attribute__ ((format (printf, 3, 4)));
void display_warning_combinable (gxine_msg_source, const gchar *, const gchar *, ...)
	__attribute__ ((format (printf, 3, 4)));
void display_info_combinable (gxine_msg_source, const gchar *, const gchar *, ...)
	__attribute__ ((format (printf, 3, 4)));

#define round_second(TIME) \
  ({ \
    int _time = (TIME); \
    if (_time % 1000 >= 500) _time += 1000; \
    _time - (_time % 1000); \
  })

void int_to_timestring (gint int_time, char* string_time, gint length);
int parse_timestring (const char *string_time);

/* Note: result of these functions needs to be freed */
gchar *modal_file_dialog (const char *title, gboolean loading, gboolean local,
			  gboolean uri, const char *pattern, GtkWidget *extra,
			  GtkWidget *parent);
GSList *modal_multi_file_dialog (const char *title, gboolean local,
				 const char *pattern, GtkWidget *parent);
void get_mrl_from_filesystem (GtkWindow *, GtkEntry *);

void window_show (GtkWidget *widget, GtkWidget *parent); /* NULL => app */
void window_present (GtkWidget *widget, GtkWidget *parent); /* ditto */

/* attach to delete-event signal -> hide widget & set *visible = FALSE */
void hide_on_delete (GtkWidget *widget, gboolean *visible);

void add_table_row_items (GtkWidget *table, gint row, gboolean expand,
			  const gchar *labeltext, ...)
			  __attribute__ ((sentinel));

gchar *asreprintf (gchar **str, const char *fmt, ...)
        __attribute__ ((format (printf, 2, 3)));

#ifdef EXP_STUFF
char *unique_name (char *base);
#endif

void do_pending_events (void);

#define foreach_glist(var,list) for (var = (list); var; var = var->next)

const char *get_copyright_notice (void);

void config_update_default (const char *key, int value);

const char *const *gxine_list_video_output_plugins (xine_t *xine);

#endif
