/*
 * Copyright (C) 2002-2006 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * nice black information display areas
 */
#ifndef GXINE_INFO_WIDGETS_H
#define GXINE_INFO_WIDGETS_H

#include <gtk/gtk.h>

typedef GtkAlignment GtkGxineInfo;
typedef GtkAlignmentClass GtkGxineInfoClass;

GtkWidget *create_infobar (gboolean small);
GtkWidget *create_time_widget (gboolean small, const GSList *const *const);

void gxineinfo_update_line (const GSList *, guint, const char *, ...)
  __attribute__ ((format (printf, 3, 4)));
void gxineinfo_update (const GSList *);
void gxineinfo_clear (const GSList *);

void infobar_show_metadata (const GSList *);

#endif
