/*
 * Copyright (C) 2002-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * init xine engine, set up script engine with player object
 */

#ifndef GXINE_ENGINE_H
#define GXINE_ENGINE_H

#include "script_engine.h"

void engine_init (void);
void engine_startup (void);
void engine_startup_script (void);
void save_startup_script (void);

int engine_exec (const char *cmd, se_print_cb_t, void *cb_data, const char *src);
int v_engine_exec (const char *, se_print_cb_t, void *, const char *, ...)
	__attribute__ ((format (printf, 1, 5)));

int engine_exec_obj (const char *, se_o_t *, se_print_cb_t, void *,
		     se_error_cb_t, const char *);
int v_engine_exec_obj (const char *, se_o_t *, se_print_cb_t, void *,
		       se_error_cb_t, const char *, ...)
	__attribute__ ((format (printf, 1, 7)));

#define engine_exec_ext(CMD,PRINT,PDATA,ERR,SRC) \
  engine_exec_obj ((CMD), NULL, (PRINT), (PDATA), (ERR), (SRC))
#define v_engine_exec_ext(CMD,PRINT,PDATA,ERR,SRC,...) \
  engine_exec_obj ((CMD), NULL, (PRINT), (PDATA), (ERR), (SRC), ## __VA_ARGS__)

void engine_queue_push (const char *cmd, se_o_t *this, se_print_cb_t cb,
			void *cb_data, se_error_cb_t ecb, const char *src);

#endif
