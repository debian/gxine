/*
 * Copyright (C) 2001-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 * Functions to stup and deal with a preferences dialog interfacing with
 * the xine config system.
 *
 * Richard Wareham <richwareham@users.sourceforge.net> -- March 2002
 */

#ifndef GXINE_PREFERENCES_H
#define GXINE_PREFERENCES_H

void preferences_init (void);
void preferences_show (void);
void preferences_update_entry (const xine_cfg_entry_t *);

#endif
