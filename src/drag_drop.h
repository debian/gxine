/*
 * Copyright (C) 2003-2006 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * drag and drop support functions
 */

#ifndef GXINE_DRAG_DROP_H
#define GXINE_DRAG_DROP_H

typedef struct {
  GtkTreeModel *model;
  GtkTreePath *path;
  int index;
} drag_mrl_t;

extern drag_mrl_t drag_mrl;

int dnd_add_mrls (GtkSelectionData *, int *current, int ins_pos);

void treeview_drag_drop_setup (GtkTreeView *,
			       void (*drop_cb) (GtkTreeView *, GdkDragContext *,
						gint, gint, GtkSelectionData *,
						guint, guint));
int treeview_get_drop_index (GtkTreeView *, GdkDragContext *, gint x, gint y);

void drag_drop_setup (GtkWidget *, gboolean autoplay);

#endif
