/*
 * Copyright (C) 2005-2006 the xine-project
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 * windows, generated from XML and using JS and gxine's .gtkrc
 */

#ifndef GXINE_XML_WIDGETS_H
#define GXINE_XML_WIDGETS_H

#include <glib.h> // gboolean
#include <gtk/gtk.h>

GtkWidget *widget_create_from_xml (const char *file, const char *name,
				   const char *title, gboolean window,
				   gboolean warn_missing);

#endif
