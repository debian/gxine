/*
 * Copyright (C) 2008 the xine project
 *
 * This file is part of xine, a free video player.
 *
 * xine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * xine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 *
 */

#ifndef GXINE_WATCHDOG_H
#define GXINE_WATCHDOG_H

#include "config.h"

#ifdef WITH_WATCHDOG
# define watchdog_off() alarm (0)
# define watchdog_on()  alarm (30)
#else
# define watchdog_off() do {} while (0)
# define watchdog_on()  do {} while (0)
#endif

#endif
