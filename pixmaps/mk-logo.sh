#! /bin/sh -e
convert logo.png -resize 320x240 -quality 9 tmp.png
png2yuv -n 1 -I p -f 25 -j tmp.png | mpeg2enc -n p -f 3 -a 2 -q 4 -b 10000 -o logo.mpv
rm tmp.png
