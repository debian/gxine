
  gxine, a GTK+ frontend for xine
  ===============================

(C) 2001-2004 Guenter Bartsch
(C) 2004-2007 Darren Salt
(C) 2001-2017 the xine project team

gxine is free software released under the GPL.


About
-----

gxine is a GTK+ based GUI for the xine-lib video player library. It is a
media player that can play all the audio/video formats that libxine
supports. Currently, this includes MPEG1/2, some AVI and Quicktime files,
some network streaming methods and disc based media (VCD, SVCD, DVD).
A more complete list can be found on http://www.xine-project.org/.

* Example files referenced in the following text may be found in the misc/
  directory in gxine's source or in the documentation directory, typically
  /usr/share/doc/gxine if you're using a packaged version.


General use
-----------

A few things which may not be immediately obvious:

- You can hide and show the toolbar by clicking the middle mouse button over
  the video display. (In windowed mode, you need to be using the separate
  toolbar for this to work.)

- You can always access the menus by right-clicking over the video panel.

You need a window manager which supports full-screen windows. If you're
using one which doesn't, full-screen mode won't work.

If you're using xcompmgr or a window manager which supports compositing,
you'll need to ensure that Xv colourkey autopainting is disabled (and you
may need to restart gxine).
  File -> Configure -> Preferences: video.device.xv_colorkey

As of 0.6.0, gxine will prompt for authentication for http:// MRLs if needed.
("http://user:pass@site/path" was previously the only way to do this.)

Currently, storage of the authentication details is insecure: it is stored
as plain text in the playlist file, and the playlist file (when saved) is
subject to the umask setting, which is normally 022 or 002 - both of which
allow it to be read by anybody.


LIRC support
------------

gxine has full LIRC support. You need to have either of
  ~/.gxine/lircrc
  ~/.lircrc
to use it. The files are read in that order; definitions are *not* overridden.

An example lircrc file is included. You'll almost certainly want to modify it.

You can let gxine execute any Javascript code you want on lirc input :-)


Javascript
----------

gxine is almost entirely controllable via Javascript. You can bind keys to
Javascript fragments (single commands, sequences of commands) - have a look
at the key bindings window (accessed via the File menu) for more
information.

As of 0.5.0, there's also support for startup scripts - one global, one
user-defined. Both are run, the global script first, when you start gxine.

Here's an example which provides independent zoom settings for windowed
and full-screen modes. You'll need to modify your keymap as described.

  // swap_zoom() suggested usage:
  //   Windowed mode:
  //     if (vo_fullscreen.v) swap_zoom (); vo_fullscreen.v = false;
  //   Fullscreen mode:
  //     swap_zoom (); vo_fullscreen.toggle ();
  function swap_zoom ()
  {
    var tmp = vo_zoom.v;
    vo_zoom.v = zoom_other;
    zoom_other = tmp;
  }
  var zoom_other = 100;

There's a Javascript reference at
  http://developer.mozilla.org/en/docs/JavaScript

As of 0.6.0, various deprecated functions are obsolete and are not included
by default. You can enable them with --enable-obsolete, but this is not
recommended; you should migrate to the vo_* and ao_* objects.

Obsolete functions will be removed and deprecated functions obsoleted in 2008.


VDR support
-----------

As of 0.4.0, gxine has VDR support. You'll need vdr, vdr-xine and a
suitably-patched xine-lib (see the vdr-xine documentation for details) to
make use of this.

If ~/.gxine already exists and contains a key bindings file from an older
version of gxine:

 * Your key bindings be upgraded automatically. Any bindings with command
   strings which don't match one of the defaults or which already have
   descriptions (also new in 0.4.0) will be left untouched - you'll have to
   check these ones yourself.

 * There are some new bindings for use with VDR. These will be added at the
   same time.


GNOME VFS support
-----------------

You'll get this automatically if the GNOME VFS library is present on your
system.


GTK themes
----------

If you wish to use different speaker icons, you must provide button-sized
images (normally 20x20) for stock items "gxine-media-speaker" and
"gxine-media-speaker-mute". The direction, size and state settings should be
wildcarded.

Also present are some "gxine-settings-*" items: "av-sync", "hue",
"saturation", "contrast", "brightness", "volume", "compressor", "amplifier".

The appearance of the time widget and info bar may be controlled via any of
the following (highest priority last):
  gxine's default gtkrc file (typically /usr/share/gxine/gtkrc)
  gxine's gtkrc conf file (typically /etc/gxine/gtkrc)
  ~/.gtkrc-2.0
  ~/.gxine/gtkrc


Toolbar and keypad windows
--------------------------

gxine has three files which define the layout and content of the toolbars
(windowed and full-screen modes are handled separately) and the keypad
window. You can override the defaults by providing

  ~/.gxine/keypad.xml
  ~/.gxine/toolbar-fullscreen.xml
  ~/.gxine/toolbar-window.xml

These files may contain translatable labels and tooltips; they are prefixed
with "~". (You probably won't need this.)

The keypad definition is a full window definition; the other definitions
require a single non-window container at the top level. The menu bar and
video pane are fixed features.

(Full documentation of these files is yet to be written.)


Screen blanking
---------------

Blanking is allowed to happen while there is no media being played or while
the media stream is paused. The display will be unblanked (or, rather,
unblanking will be triggered) when playback is started or resumed.

gxine tries various methods to (temporarily) defeat screen savers, so if you
see a message something like "can't poke gnome-screensaver", it's almost
certainly nothing to worry about.


Bug reports and feature requests
--------------------------------

* If you installed gxine from a distribution:

  You should report problems and request features via your distribution's bug
  tracking system.

* If you installed gxine from source fetched from Sourceforge:

  Check that you're using a recent version and that the problem is still
  present in that version. If you are or it is, then you should report the
  problem. To do this, visit http://bugs.xine-project.org/.

* If you installed gxine from a third-party archive:

  You should contact that archive's maintainer.


If you find that your problem or missing feature has already been reported,
there's no point in re-reporting it, but if you have any extra information
which may help in fixing the problem, you should add that to the existing
report.

If you're not sure what to do, you can always post to the xine-user mailing
list. (But do search the list archive first.)
