
  gxine, GTK+ frontend pro xine
  =============================

(C) 2001-2004 Guenter Bartsch
(C) 2004-2007 Darren Salt
(C) 2001-2017 tým projektu xine

gxine je svobodný software vydaný pod GPL.


O programu
----------

gxine je grafické rozhraní pro knihovnu videopřehrávače xine-lib používající
GTK+. Jedná se o přehrávač médií, který umožňuje přehrávat všechny zvukové
a video formáty, které podporuje libxine. V této chvíli je to MPEG1/2, některé
soubory AVI a Quicktime, několik metod síťového streamování a disková média
(VCD, SVCD, DVD). Ucelenější seznam naleznete na http://www.xine-project.org/.

* Soubory příkladů zmíněné v následujícím textu jsou k nalezení ve zdrojových
  kódech gxine v adresáři misc/ nebo v adresáři s dokumentací, typicky
  /usr/share/doc/gxine.


Obecné použití
--------------

Několik věcí, které nemusejí být hned zřejmé:

- Můžete zobrazit nebo skrýt nástrojovou lištu kliknutím prostředního tlačítka
  myši na videu (v okenním režimu musíte použít oddělenou nástrojovou lištu).

- Můžete vždy přistupovat k menu kliknutím pravého tlačítka na panelu videa.

Potřebujete okenní manažer, který podporuje celoobrazovková okna. Pokud
používáte některý, který to nepodporuje, režim na celé obrazovce nebude
fungovat.

Pokud používáte xcompmgr nebo okenní manažer podporující compositing, musíte se
ujistit, že je zakázáno automatické vykreslování klíčové barvy Xv (k tomu
může být potřeba restartovat gxine).
  Soubor -> Konfigurace -> Vlastnosti: video.device.xv_colorkey


Podpora LIRC
------------

gxine má plnou podporu LIRC. Abyste ho mohli použít, potřebujete jeden ze
souborů:
  ~/.gxine/lircrc
  ~/.lircrc
Soubory jsou načítány v tomto pořadí, definice *nejsou* přepisovány.

Obsažen je příklad souboru lircrc. Téměř jistě ho budete chtít upravit.

Na základě vstupu LIRC můžete nechat gxine vykonat jakýkoliv Javaskriptový kód.
:-)


Javaskript
----------

gxine je téměř úplně ovladatelné pomocí Javaskriptu. Můžete přiřadit klávesám
části Javaskriptového kódu (jeden příkaz, sadu příkazů) - více informací
naleznete v okně klávesových zkratek (přístupného z položky menu "Soubor").

Od verze 0.5.0 je zde podpora startovacích skriptů. Jeden hlavní, jeden
definovaný uživatelem. Oba skripty se spouští při spuštění gxine - nejprve
hlavní, potom uživatelský.

Zde je příklad, který umožní nezávislé nastavení zvětšení pro okenní a
celoobrazovkový režim. Změňte vaši mapu kláves tak, jak je popsáno:

  // swap_zoom() navrhované použití:
  //   Okenní režim:
  //     if (vo_fullscreen.v) swap_zoom (); vo_fullscreen.v = false;
  //   Celoobrazovkový režim:
  //     swap_zoom (); vo_fullscreen.toggle ();
  function swap_zoom ()
  {
    var tmp = vo_zoom.v;
    vo_zoom.v = zoom_other;
    zoom_other = tmp;
  }
  var zoom_other = 100;

Referenční dokumentace k Javascriptu je zde:
  http://developer.mozilla.org/en/docs/JavaScript


Podpora VDR
-----------

Od verze 0.4.0 gxine podporuje VDR. Abyste ho mohli použít, budete potřebovat
vdr, vdr-xine a vhodně ozáplatované xine-lib (detaily viz dokumentace
k vdr-xine).

Pokud již ~/.gxine existuje a obsahuje soubor klávesových zkratek starší verze
gxine, pak:

 * Vaše klávesové zkratky budou automaticky aktualizovány. Jakékoliv zkratky
   s řetězci příkazů, které neodpovídají některému přednastavenému nebo které
   mají popisy (také nově ve verzi 0.4.0), budou ponechány beze změny - budete
   je muset zkontrolovat sami.

 * Objevili se také nové zkratky pro použití VDR. Ty se při aktualizaci
   přidají.


Podpora GNOME VSF
-----------------

Bude automaticky, pokud je ve vašem systému přítomna knihovna GNOME VFS.


Témata GTK
----------

Pokud si přejete použít jiné ikony reproduktorů, musíte pro GTK položky dodat
obrázky o velikosti tlačítek (obvykle 20x20) "gxine-media-speaker" a
"gxine-media-speaker-mute". Nastavení orientace, velikosti a stavů by nemělo
být měněno (použitím "žolíků").

K dispozici jsou také některé položky gxine-settings-*: "av-sync", "hue",
"saturation", "contrast", "brightness", "volume", "compressor", "amplifier".

Vzhled časového posuvníku a informačního baneru lze řídit kterýmkoliv
z následujících způsobů (nejvyšší priorita jako poslední):
  výchozí soubor gtkrc (typicky /usr/share/gxine/gtkrc)
  konfigurační soubor gtkrc (typicky /etc/gxine/gtkrc)
  ~/.gtkrc-2.0
  ~/.gxine/gtkrc


Nástrojové a klávesové okno
---------------------------

gxine má tři soubory, které definují rozvržení a obsah nástrojových lišt
(okenní a celoobrazovková jsou chápány odděleně) a klávesového okna. Výchozí
hodnoty můžete přenastavit v souborech:

  ~/.gxine/keypad.xml
  ~/.gxine/toolbar-fullscreen.xml
  ~/.gxine/toolbar-window.xml

Tyto soubory mohou obsahovat přeložitelné jmenovky a nástrojové tipy. Mají
předponu "~", ale pravděpodobně toto nebudete potřebovat.

Klávesová definice (keypad.xml) je definice pro celou obrazovku. Ostatní
definice vyžadují na nejvyšší úrovni jednoduchý neokenní container. Menu a
panel videa jsou neměnné prvky.

(Plná dokumentace k těmto souborům musí být teprve napsána.)


Oznamování chyb a požadavky na funkce
-------------------------------------

* Pokud jste instalovali gxine z distribuce:

  Měli byste oznamovat problémy a požadavky na funkce pomocí bug tracking
  systému vaší distribuce.

* Pokud jste instalovali gxine ze zdrojových kódů stažených z Sourceforge:

  Zkontrolujte, zda používáte poslední verzi a že se problém v této verzi
  ještě vyskytuje. Pokud ano, měli byste problém oznámit. Navštivte
  http://bugs.xine-project.org/.

* Pokud jste instalovali gxine z archívu třetí strany:

  Měli byste kontaktovat správce archívu.


Pokud zjistíte, že váš problém nebo chybějící funkce již byl oznámen, není
žádný důvod k opětovnému oznamování. Ale pokud máte jakoukoliv další informaci,
která by mohla pomoci opravit problém, měli byste ji přidat k existujícímu
oznámení.

Pokud si nejste jisti, co dělat, můžete vždy napsat do mailového listu
xine-user. (Ale nejdříve ho prohledejte.)
