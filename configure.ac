AC_PREREQ(2.62)
AC_INIT([gxine], [0.5.910])
AC_CONFIG_SRCDIR([src/main.c])

AC_CANONICAL_HOST
AC_CONFIG_HEADERS([include/config.h])
AM_INIT_AUTOMAKE([1.11 dist-bzip2 no-dist-gzip -Wall])
AM_MAINTAINER_MODE
dnl AM_ACLOCAL_INCLUDE(m4)

m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])

AC_GNU_SOURCE

XINE_LIB_MIN_VER=1.1.8

dnl ---------------------------------------------
dnl Check for programs.
dnl ---------------------------------------------
dnl Save CFLAGS, AC_ISC_POSIX set some unwanted default CFLAGS
saved_CFLAGS="$CFLAGS"
AC_ISC_POSIX
CFLAGS="$saved_CFLAGS"
AM_PROG_CC_C_O
AC_HEADER_STDC
dnl AC_ARG_PROGRAM
m4_ifdef([AM_PROG_AR], [AM_PROG_AR])

dnl ---------------------------------------------
dnl Libtool
dnl ---------------------------------------------
m4_undefine([AC_PROG_CXX])
m4_defun([AC_PROG_CXX],[])
m4_undefine([AC_PROG_F77])
m4_defun([AC_PROG_F77],[])
AC_LIBTOOL_DLOPEN
AM_DISABLE_STATIC
AC_PROG_LIBTOOL
AC_SUBST(LIBTOOL_DEPS)
AC_LD_ASNEEDED

dnl
dnl Windows
dnl

case "$host_os" in
    *mingw32*)
        WIN32_SYS=mingw32
        LIBS="-lwsock32 $LIBS"
        AC_DEFINE([_WIN32_WINNT], 0x0502, [Define to '0x0502' for Windows XP SP2 APIs.])
        CPPFLAGS="${CPPFLAGS} -D__USE_MINGW_ANSI_STDIO=1"
        ;;
esac

AM_CONDITIONAL([WIN32], [test x"$WIN32_SYS" = x"mingw32"])

dnl ---------------------------------------------
dnl Checks for typedefs, structures, and compiler characteristics.
dnl ---------------------------------------------
AC_C_CONST
AC_C_INLINE
AC_TYPE_OFF_T
AC_TYPE_SIZE_T
AC_CHECK_TYPES([ptrdiff_t])
AC_C_BIGENDIAN
AC_C_ALWAYS_INLINE

saved_CFLAGS="$CFLAGS"
AC_MSG_CHECKING([whether $CC supports ?: with no middle operand])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[int i = 1 ? : 2;])],
	AC_MSG_RESULT(yes),
	AC_MSG_RESULT(no)
	AC_MSG_ERROR([\?: with no middle operand is required]))
AC_MSG_CHECKING([how to specify C99 with GNU extensions for $CC])
ac_cv_c_gnu99=no
for ac_cv in -std=gnu99; do
  CFLAGS="$saved_CFLAGS $ac_cv"
  AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[])],[ac_cv_c_gnu99=$ac_cv; break],)
done
AC_MSG_RESULT($ac_cv_c_gnu99)
if test "$ac_cv_c_gnu99" = "no"; then
  AC_MSG_WARN([there may be build problems])
fi
AC_SUBST(GNU99,$ac_cv_c_gnu99)
CFLAGS="$saved_CFLAGS"

AC_CHECK_HEADERS([alloca.h])
AC_CHECK_HEADERS([poll.h netdb.h])
AC_CHECK_HEADERS([netinet/in.h])
AC_CHECK_HEADERS([sys/un.h sys/socket.h])

dnl ---------------------------------------------
dnl gettext
dnl ---------------------------------------------
AM_GNU_GETTEXT([external])
AC_SUBST([XGETTEXT_EXTRA_OPTIONS], [--add-comments])

dnl ---------------------------------------------
dnl libm, libdl
dnl ---------------------------------------------
AC_CHECK_LIB(m, round, [], [], [-lm])
AC_CHECK_LIB(dl, dlsym, [], [], [-ldl])

dnl ---------------------------------------------
dnl Checks for X11 and Xcb
dnl ---------------------------------------------

GXINE_CHECK_X11_XCB

dnl ---------------------------------------------
dnl socket library
dnl ---------------------------------------------

dnl Test for socket and network support library
NET_LIBS=""
AC_CHECK_LIB(socket, socket, NET_LIBS="-lsocket $NET_LIBS",)
AC_CHECK_LIB(nsl, gethostbyname, NET_LIBS="-lnsl $NET_LIBS",)
AC_SUBST(NET_LIBS)

dnl ---------------------------------------------
dnl Check for libsmjs (spidermonkey)
dnl ---------------------------------------------

GXINE_PATH_SPIDERMONKEY

dnl ---------------------------------------------
dnl Check whether to build the Moz browser plugin
dnl ---------------------------------------------
AC_MSG_CHECKING(whether to build the Mozilla plugin)
AC_ARG_WITH(browser-plugin,
	    AS_HELP_STRING(--without-browser-plugin,
			   [do not build the Mozilla browser plugin]),
	    if test "$withval" = no; then browser_plugin=no; else browser_plugin=yes; fi,
	    browser_plugin=yes)
AC_MSG_RESULT($browser_plugin)
if test "$browser_plugin" = yes; then
  saved_CPPFLAGS=$CPPFLAGS
  CPPFLAGS="$CPPFLAGS $X_CFLAGS"
  AC_CHECK_HEADER(X11/Xaw/Form.h,,
       [AC_MSG_ERROR([you need to install libxaw header files (-dev package)])],
       [#include <X11/Intrinsic.h>])
  CPPFLAGS=$saved_CPPFLAGS
  GXINE_PATH_NSPR
fi
AM_CONDITIONAL([BROWSER_PLUGIN], [test "$browser_plugin" = yes])

dnl ---------------------------------------------
dnl check for gtk 2.18 or later or gtk 3.0
dnl ---------------------------------------------
AC_ARG_WITH([gtk3], AS_HELP_STRING([--with-gtk3], [use GTK+ 3]), [force_gtk3=yes; force_gtk2=no])
if test "x$force_gtk3" = 'xyes'; then
  PKG_CHECK_MODULES(GTK3, gtk+-3.0 >= 3.0.0)
else
  PKG_CHECK_MODULES(GTK2, gtk+-2.0 >= 2.18.0)
fi

PKG_CHECK_MODULES(GLIB2, glib-2.0 >= 2.32.0)
PKG_CHECK_MODULES(GTHREAD2, gthread-2.0 >= 2.10.0)
PKG_CHECK_MODULES(PANGO, pango >= 1.12.0)
saved_CFLAGS="$CFLAGS"
saved_LIBS="$LIBS"
CFLAGS="$CFLAGS $GLIB2_CFLAGS"
LIBS="$LIBS $GLIB2_LIBS"
AC_CACHE_CHECK([whether glib uses the system malloc by default],
  gxine_cv_glib_system_malloc,
  AC_RUN_IFELSE([AC_LANG_SOURCE([
#include <glib.h>
int main ()
{
  return g_mem_is_system_malloc () ? 0 : 'n';
}
])],
    gxine_cv_glib_system_malloc=yes,
    [if test $? != 110; then
	AC_MSG_FAILURE([failed to run test])
     fi
     gxine_cv_glib_system_malloc=no
    ],
    gxine_cv_glib_system_malloc=no),
)
if test "$gxine_cv_glib_system_malloc" = "yes"; then
  AC_DEFINE(GLIB_USES_SYSTEM_MALLOC, 1,
	    [Define if glib uses the system malloc by default.])
fi
CFLAGS="$saved_CFLAGS"
LIBS="$saved_LIBS"

dnl ---------------------------------------------
dnl HAL or UDEV (for hardware information)
dnl ---------------------------------------------

AC_ARG_WITH([gudev],
	AS_HELP_STRING([--with-gudev], [enable gudev support (for device info etc.)]),
	[requested=Y], [requested=N])
if test "x$with_gudev" != 'xno'; then
  if test "x$requested" = "xY"; then
    PKG_CHECK_MODULES(UDEV, gudev-1.0)
  else
    PKG_CHECK_MODULES(UDEV, gudev-1.0, [], [with_gudev=no; AC_MSG_RESULT([no])])
  fi
fi
if test "$with_gudev" = yes; then
  AC_DEFINE(WITH_GUDEV, 1, [Define if libgudev is present and you want to use it.])
fi

AC_ARG_WITH([hal],
	AS_HELP_STRING([--without-hal], [disable HAL support (for device info etc.)]),
	[requested=Y], [requested=N])
if test "x$with_hal" != 'xno'; then
  with_hal=yes
  if test "$with_gudev" = yes; then
    with_hal=no
  elif test "x$requested" = "xY"; then
    PKG_CHECK_MODULES(HAL, hal)
  else
    PKG_CHECK_MODULES(HAL, hal, [], [with_hal=no; AC_MSG_RESULT([no])])
  fi
fi
if test "$with_hal" = yes; then
  AC_DEFINE(WITH_HAL, 1, [Define if HAL is present and you want to use it.])
  GXINE_HAL_DEVICE_INFO
fi

dnl ---------------------------------------------
dnl Screensaver commands
dnl ---------------------------------------------
AC_ARG_VAR([XDGSSCMD], [full pathname for xdg-screensaver (disabled, 'yes' to auto-detect)])
AC_ARG_VAR([GSSCMD], [full pathname for gnome-screensaver-command (auto-detect, 'no' to disable)])
AC_ARG_VAR([XSSCMD], [full pathname for xscreensaver-command (auto-detect, 'no' to disable)])

dnl Disable XDG screensaver by default
if test "$XDGSSCMD" = ''; then
  XDGSSCMD=no
fi

GXINE_SCREENSAVER_CMD([XDGSSCMD], [XDG], [xdg-screensaver])
GXINE_SCREENSAVER_CMD([GSSCMD], [GNOME], [gnome-screensaver],
   [AC_ARG_WITH([dbus],
		AS_HELP_STRING([--without-dbus], [disable dbus support (for GNOME screensaver)]),
		[requested=Y], [requested=N])
    if test "x$with_dbus" != 'xno'; then
      with_dbus=yes
      if test "x$requested" = "xY"; then
	PKG_CHECK_MODULES([DBUS], [dbus-glib-1])
      else
	PKG_CHECK_MODULES([DBUS], [dbus-glib-1], [], [with_dbus=no; AC_MSG_RESULT([no])])
      fi
    fi
    if test "$with_dbus" = yes; then
      AC_DEFINE([WITH_DBUS], [1], [Define if dbus-glib is present and you want to use it to talk to gnome-screensaver.])
    fi
    ])
GXINE_SCREENSAVER_CMD([XSSCMD], [X], [xscreensaver-command])

dnl ---------------------------------------------
dnl X11 (required)
dnl ---------------------------------------------

X_LIBS="-lX11 $X_LIBS"

dnl ---------------------------------------------
dnl DPMS Extension
dnl ---------------------------------------------
ac_have_dpms=yes
XEXT_LIBS=''
AC_CHECK_LIB(Xext, DPMSQueryExtension,
	[AC_CHECK_HEADER([X11/extensions/dpms.h],
		[XEXT_LIBS=-lXext
		 AC_DEFINE(HAVE_DPMS_EXTENSION,,[Define this to use DPMS to defeat the screen blanker])],
		[ac_have_dpms=no],
		[#include <X11/Xlib.h>])],
	[ac_have_dpms=no],
	[$X_LIBS $X_PRE_LIBS -lXext $X_EXTRA_LIBS])
AC_SUBST(XEXT_LIBS)
AM_CONDITIONAL([HAVE_DPMS_EXTENSION], [test $ac_have_dpms = yes])

dnl ---------------------------------------------
dnl XTest Extension
dnl ---------------------------------------------
ac_have_xtest=no
AC_ARG_ENABLE(defeat-screensaver-via-xtest,
	    AS_HELP_STRING(--enable-defeat-screensaver-via-xtest,
			   [enable defeating the screen blanker via fake key presses (if XTest is supported)]),
	    if test "x$enableval" = xno; then xtest=no; else xtest=yes; fi,
	    xtest=no)
if test "x$xtest" != xno; then
  XTEST_LIBS=''
  AC_CHECK_LIB(Xtst, XTestFakeKeyEvent,
	[AC_CHECK_HEADER([X11/extensions/XTest.h],
		[XTEST_LIBS=-lXtst
		 AC_DEFINE(HAVE_XTESTEXTENSION,,[Define this to use XTest to defeat the screen blanker (requires libXtst)])],
		[ac_have_xtest=no])],
	[ac_have_xtest=no],
	[$X_LIBS $X_PRE_LIBS -lXext $X_EXTRA_LIBS])
  AC_SUBST(XTEST_LIBS)
else
  ac_have_xtest=no
fi
AM_CONDITIONAL([HAVE_XTESTEXTENSION], [test $ac_have_xtest = yes])

dnl ---------------------------------------------
dnl Xinerama Extension
dnl ---------------------------------------------
AC_ARG_WITH([xinerama],
	AS_HELP_STRING([--without-xinerama], [disable Xinerama support (not recommended)]),
	[requested=Y], [requested=N])
if test "x$with_xinerama" != "xno"; then
  saved_CFLAGS="$CFLAGS"
  saved_LIBS="$LIBS"
  CFLAGS="$CFLAGS $X_CFLAGS $GTK2_CFLAGS $GTK3_CFLAGS"
  ac_have_xinerama=yes
  XINERAMA_LIBS=''
  AC_CHECK_LIB(Xinerama, XineramaQueryExtension,
	[AC_CHECK_HEADER([X11/extensions/Xinerama.h],
		XINERAMA_LIBS=-lXinerama
		[AC_DEFINE(HAVE_XINERAMA,,[Define this if you have libXinerama])],
		[ac_have_xinerama=no])],
	[ac_have_xinerama=no],
	[$X_LIBS $X_PRE_LIBS -lXext $X_EXTRA_LIBS])
  AC_SUBST(XINERAMA_LIBS)
  test $requested = Y && test $ac_have_xinerama = no &&
	AC_MSG_ERROR([xinerama was requested but not found])
  CFLAGS="$saved_CFLAGS"
  LIBS="$saved_LIBS"
else
  ac_have_xinerama=no
fi
AM_CONDITIONAL([HAVE_XINERAMA], [test $ac_have_xinerama = yes])

dnl ---------------------------------------------
dnl XRandR Extension (check via GDK)
dnl ---------------------------------------------
saved_CFLAGS="$CFLAGS"
saved_LIBS="$LIBS"
CFLAGS="$CFLAGS $X_CFLAGS $GTK2_CFLAGS $GTK3_CFLAGS"
XRANDR_LIBS=''
AC_CHECK_LIB(Xrandr, XRRQueryExtension,
	     [XRANDR_LIBS=-lXrandr
	      AC_DEFINE(HAVE_XRANDR,,[Define this if you have libXrandr installed (and GTK+2 uses it)])
	      ac_have_xrandr=yes],
	     [ac_have_xrandr=no],
	     [$X_LIBS $X_PRE_LIBS $GTK2_LIBS $GTK3_LIBS $X_EXTRA_LIBS])
CFLAGS="$saved_CFLAGS"
LIBS="$saved_LIBS"
AC_SUBST(XRANDR_LIBS)
AM_CONDITIONAL([HAVE_XRANDR], [test x$ac_have_xrandr = "xyes"])

dnl ---------------------------------------------
dnl threads
dnl ---------------------------------------------

case $host in
  *-*-freebsd*)
    THREAD_LIBS="-pthread"
    CFLAGS="$CFLAGS -I/usr/local/include -L/usr/local/lib -D_THREAD_SAFE"
    CPPFLAGS="$CPPFLAGS -I/usr/local/include"
    LDFLAGS="$LDFLAGS -L/usr/local/lib"
    ;;
  *)
   THREAD_LIBS="-lpthread"
    AC_CHECK_LIB(pthread, pthread_create,
		[AC_CHECK_HEADER([pthread.h],,[AC_MSG_ERROR(pthread needed)])],
		[AC_MSG_ERROR(pthread needed)])
    ;;
esac
AC_SUBST(THREAD_LIBS)

dnl ---------------------------------------------
dnl Check for xine-lib
dnl ---------------------------------------------
AM_PATH_XINE([$XINE_LIB_MIN_VER],, AC_MSG_ERROR([*** Please install xine-lib (devel) first ***]))

dnl Check for xine-list-* (MIME types)
XINE_LIST="$(pkg-config --variable=xine_list libxine 2>/dev/null || xine-config --xine-list 2>/dev/null)"
AM_CONDITIONAL([XINE_LIST], [test "$XINE_LIST" != ''])
AC_SUBST(XINE_LIST)

dnl Extract xine-lib's text domain name
LIB_PACKAGE="libxine`echo '#define ENABLE_NLS
  #include <xine/xine_internal.h>
  XINE_MAJOR_VERSION' | $CPP $CPPFLAGS $XINE_CFLAGS - | sed -e '$!d; s/^ \+//'`" 2>/dev/null
if test "$LIB_PACKAGE" = 'libxine'; then
  AC_MSG_ERROR([Couldn't determine xine-lib's text domain name])
fi
AC_DEFINE_UNQUOTED(LIB_PACKAGE,"$LIB_PACKAGE",[xine-lib's text domain name])

CC_CHECK_CFLAGS_APPEND(["-Wall -Wno-unused-parameter"])
CC_CHECK_CFLAGS_APPEND(["-Wmissing-format-attribute -Wmissing-declarations -Wmissing-prototypes"])
CC_CHECK_CFLAGS_APPEND(["-Werror-implicit-function-declaration"])
CC_CHECK_CFLAGS_APPEND(["-Wstrict-aliasing=2"])
CC_CHECK_CFLAGS_APPEND(["-Wchar-subscripts -Wpointer-arith"])
AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

HAVE_MPEG="`test -f "$xine_plugin_dir/xineplug_dmx_mpeg_elem.$acl_cv_shlibext" && echo yes || echo no`"
HAVE_IMAGE="`test -f "$xine_plugin_dir/xineplug_dmx_image.$acl_cv_shlibext" && echo yes || echo no`"

AC_MSG_CHECKING([which logo format to use])
echo "$as_me:$LINENO: xine plugin directory: $xine_plugin_dir" >&5
echo "$as_me:$LINENO: have MPEG elementary demuxer: $HAVE_MPEG" >&5
echo "$as_me:$LINENO: have image demuxer: $HAVE_IMAGE" >&5
AC_ARG_WITH(logo-format,
	    AS_HELP_STRING(--with-logo-format,
			   [set idle-mode logo format: "mpeg", "image" or "auto"]),
	    [case "$withval" in
	      mpeg|image|auto)
		withval="$withval"
		;;
	      *)
		AC_MSG_ERROR([unrecognised logo type])
		;;
	     esac],
	    [withval=auto])
LOGO_FORMAT=''
logo_sub=''
while test "$LOGO_FORMAT" = ''; do
  echo "$as_me:$LINENO: trying type=$withval" >&5
  if test "$withval" = auto; then
    # preferred is last
    if test "$HAVE_IMAGE" = yes; then withval=image; fi
    if test "$HAVE_MPEG" = yes; then withval=mpeg; fi
    # handle missing-demuxer case
    if test "$withval" = auto; then
      AC_MSG_ERROR([no available demuxer for the logo image - rebuild/reinstall xine-lib?])
    fi
  fi
  case "$withval" in
    image)
	if test "$HAVE_IMAGE" = no; then
	  logo_sub="$withval"
	  withval=auto
	else
	  LOGO_FORMAT=jpg
	fi
	;;
    mpeg)
	if test "$HAVE_MPEG" = no; then
	  logo_sub="$withval"
	  withval=auto
	else
	  LOGO_FORMAT=mpv
	fi
	;;
  esac
done
AC_SUBST(LOGO_FORMAT)
AC_DEFINE_UNQUOTED(LOGO_FORMAT, "$LOGO_FORMAT", [Set to the logo image format (mpv or jpg)])
AC_MSG_RESULT([$withval])
if test "$logo_sub" != ''; then
  AC_MSG_WARN([logo format "$logo_sub" cannot be displayed - demuxer not present])
fi

XINE_LIB_SHIMS

dnl ---------------------------------------------
dnl Check for lirc
dnl ---------------------------------------------
AC_CHECK_LIRC

dnl ---------------------------------------------
dnl Check for GNU getopt_long()
dnl ---------------------------------------------
AC_MSG_CHECKING(for GNU getopt_long)
AC_RUN_IFELSE([AC_LANG_SOURCE([[
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

static struct option long_options[] = {
  {"help"    , no_argument, 0, 1 },
  {"version" , no_argument, 0, 2 },
  {0         , no_argument, 0, 0 }
};

int main (int argc, char **argv) {
  int option_index = 0;
  int c;
  opterr = 0;
  while ((c = getopt_long (argc, argv, "?hv",
			   long_options, &option_index)) != EOF)
    ;
  return 0;
}
	]])],
	[AC_MSG_RESULT(yes);
	 ac_getopt_long=yes;
	 AC_DEFINE(HAVE_GETOPT_LONG,,[Define this if you have GNU getopt_long() implemented])],
	[AC_MSG_RESULT(no); ac_getopt_long=no],
	[AC_MSG_RESULT(no); ac_getopt_long=no])
AM_CONDITIONAL(HAVE_GETOPT_LONG, test x"$ac_getopt_long" = "xyes")

dnl ---------------------------------------------
dnl That nice stable libmozjs API... :-(
dnl ---------------------------------------------
GXINE_JS_CHECK_TYPES

dnl ---------------------------------------------
dnl Miscellaneous build flags
dnl ---------------------------------------------

dnl AC_MSG_CHECKING([for requested GTK+ compatibility])
dnl AC_ARG_ENABLE(gtk-compat,
dnl	    AS_HELP_STRING(--disable-gtk-compat,
dnl			   [don't be compatible with older GTK+ or automatically use features from newer GTK+]),
dnl	    if test "$enableval" = no; then gtkcompat=no; else gtkcompat=yes;dnl  fi,
dnl	    gtkcompat=yes)
dnl if test "$gtkcompat" = yes; then
dnl   AC_DEFINE(GXINE_GTK_COMPAT, 1,
dnl	    [Define if you want compatibility with older GTK+ and automatic use of some features in newer GTK+])
dnl   AC_MSG_RESULT([2.2, using newer features if available at runtime])
dnl else
dnl   AC_MSG_RESULT([installed version, using no newer features (except menu icons)])
dnl fi

if test x"$with_xcb" != xno; then
  AC_MSG_CHECKING([whether to enable workaround for libXi deadlock on Solaris])
  AC_ARG_ENABLE(sun-xlib-workaround,
		AS_HELP_STRING(--enable-sun-xlib-workaround,
			       [enable workaround for libXi deadlock on Solaris]),
		if test "$enableval" = no; then sun_xlib_workaround=no; else sun_xlib_workaround=yes; fi,
		sun_xlib_workaround=no)
  if test "$sun_xlib_workaround" = yes; then
    AC_DEFINE(USE_SUN_XLIB_WORKAROUND, 1,
	      [Define if you are experiencing a hang in gxine on Solaris])
  fi
  AC_MSG_RESULT($sun_xlib_workaround)
fi

AC_MSG_CHECKING([whether to enable the watchdog (will abort gxine if it hangs)])
AC_ARG_ENABLE(watchdog,
	    AS_HELP_STRING(--enable-watchdog,
			   [enable watchdog (to abort gxine if it hangs)]),
	    if test "$enableval" = no; then watchdog=no; else watchdog=yes; fi,
	    watchdog=no)
if test "$watchdog" = yes; then
  AC_DEFINE(WITH_WATCHDOG, 1,
	    [Define if you want to enable the watchdog (will abort gxine if it hangs])
fi
AC_MSG_RESULT($watchdog)

AC_MSG_CHECKING([whether to use the integration wizard])
AC_ARG_ENABLE(integration-wizard,
	    AS_HELP_STRING(--disable-integration-wizard,
			   [don't configure other applications to use gxine (for packagers)]),
	    if test "$enableval" = no; then integrate=no; else integrate=yes; fi,
	    integrate=yes)
if test "$integrate" = yes; then
  AC_DEFINE(USE_INTEGRATION_WIZARD, 1,
	    [Define if you want gxine to be able to configure other apps to use it])
fi
AC_MSG_RESULT($integrate)

AC_MSG_CHECKING([whether to build our own playlist file parsers])
AC_ARG_ENABLE(own-playlist-parsers,
	    AS_HELP_STRING(--disable-own-playlist-parsers,
			   [rely on xine-lib's playlist parsing (feedback welcome)]),
	    if test "$enableval" = no; then own_pl_parse=no; else own_pl_parse=yes; fi,
	    own_pl_parse=yes)
if test "$own_pl_parse" = yes; then
  AC_DEFINE(PARSE_PLAYLISTS, 1,
	    [Undefine if you want gxine to use xine-lib's playlist parsing code (feedback welcome)])
fi
AC_MSG_RESULT($own_pl_parse)

AC_MSG_CHECKING([whether to include deprecated features])
AC_ARG_ENABLE(deprecated,
	    AS_HELP_STRING(--disable-deprecated,
			   [exclude deprecated features, e.g. old Javascript functions]),
	    if test "$enableval" = no; then deprecated=no; else deprecated=yes; fi,
	    deprecated=yes)
if test "$deprecated" = yes; then
  AC_DEFINE(WITH_DEPRECATED, 1,
	    [Define if you want gxine to be built with deprecated features])
fi
AC_MSG_RESULT($deprecated)

if test "$deprecated" = yes; then
  AC_MSG_CHECKING([whether to include obsolete features])
  AC_ARG_ENABLE(obsolete,
	    AS_HELP_STRING(--enable-obsolete,
			   [include obsolete features, e.g. old Javascript functions]),
	    if test "$enableval" != yes; then obsolete=no; else obsolete=yes; fi,
	    obsolete=no)
  if test "$obsolete" = yes; then
    AC_DEFINE(WITH_OBSOLETE, 1,
	    [Define if you want gxine to be built with obsolete features])
  fi
  AC_MSG_RESULT($obsolete)
fi

dnl ---------------------------------------------
dnl Debugging options
dnl ---------------------------------------------

AS_IF([test "${enable_lock_debugging+set}" = set],
      [if test "$enable_lock_debugging" = yes; then
	 CFLAGS="$CFLAGS -DLOCK_DEBUG"
	 AC_MSG_NOTICE([lock debugging is enabled])
       fi],
      [])

AS_IF([test "${enable_logging+set}" = set],
      [if test "$enable_logging" = yes; then
	 CFLAGS="$CFLAGS -DLOG"
	 AC_MSG_NOTICE([debug log output is enabled])
       fi],
      [])

dnl ---------------------------------------------
dnl where to install the browser plugin
dnl ---------------------------------------------

if test "x$prefix" = xNONE; then
   prefix="${ac_default_prefix}"
fi
if test "x$exec_prefix" = xNONE; then
   exec_prefix='${prefix}'
fi

dnl ---------------------------------------------
dnl debug cflags & ldflags
dnl ---------------------------------------------
DEBUG_CFLAGS="$CFLAGS -g -O0 -DDEBUG -D_GNU_SOURCE"
DEBUG_LDFLAGS="$LDFLAGS -g"
AC_SUBST(DEBUG_CFLAGS)
AC_SUBST(DEBUG_LDFLAGS)

dnl ---------------------------------------------
dnl PNG versions of our SVG icons
dnl ---------------------------------------------

OPTIPNG=''
AC_PATH_PROG(RSVG, rsvg)
if test "x$RSVG" != x; then
  AC_PATH_PROG(OPTIPNG, optipng)
  AC_PATH_PROG(ADVPNG, advpng)
fi
AM_CONDITIONAL(MAKE_PNG, test "x$RSVG" != x)
AM_CONDITIONAL(HAVE_OPTIPNG, [test "x$OPTIPNG" != x])
AM_CONDITIONAL(HAVE_ADVPNG, [test "x$ADVPNG" != x])


GXINE_PLUGINDIR="$libdir/gxine"
eval GXINE_PLUGINPATH=`eval echo "$GXINE_PLUGINDIR"`
AC_DEFINE_UNQUOTED(GXINE_PLUGINDIR,"$GXINE_PLUGINPATH",[where to install the browser plugin])
AC_SUBST(GXINE_PLUGINPATH)

GXINE_BINDIR="$bindir"
eval GXINE_BINPATH=`eval echo "$GXINE_BINDIR"`
AC_DEFINE_UNQUOTED(GXINE_BINDIR,"$GXINE_BINPATH",[where to install the binary])
AC_SUBST(GXINE_BINPATH)

GXINE_LOGODIR="$datadir/gxine"
eval GXINE_LOGOPATH=`eval echo "$GXINE_LOGODIR"`
AC_DEFINE_UNQUOTED(GXINE_LOGODIR,"$GXINE_LOGOPATH",[where to install the logo])
AC_SUBST(GXINE_LOGOPATH)

GXINE_PIXMAPDIR="$datadir/gxine/pixmaps"
eval GXINE_PIXMAPPATH=`eval echo "$GXINE_PIXMAPDIR"`
AC_DEFINE_UNQUOTED(GXINE_PIXMAPDIR,"$GXINE_PIXMAPPATH",[where to install pixmaps])
AC_SUBST(GXINE_PIXMAPPATH)

GXINE_ICONDIR="$datadir/pixmaps"
eval GXINE_ICONPATH=`eval echo "$GXINE_ICONDIR"`
AC_DEFINE_UNQUOTED(GXINE_ICONDIR,"$GXINE_ICONPATH",[where to install desktop icons])
AC_SUBST(GXINE_ICONPATH)

GXINE_HICOLOURPATH_REL="icons/hicolor/64x64/apps"
GXINE_HICOLOURDIR="$datadir/$GXINE_HICOLOURPATH_REL"
eval GXINE_HICOLOURPATH=`eval echo "$GXINE_HICOLOURDIR"`
AC_DEFINE_UNQUOTED(GXINE_HICOLOURDIR,"$GXINE_HICOLOURPATH",[where to install high-colour desktop icons])
AC_SUBST(GXINE_HICOLOURPATH)
AC_SUBST(GXINE_HICOLOURPATH_REL)

GXINE_MISCDIR="$datadir/gxine"
eval GXINE_MISCPATH=`eval echo "$GXINE_MISCDIR"`
AC_DEFINE_UNQUOTED(GXINE_MISCDIR,"$GXINE_MISCPATH",[where to install misc files (e.g. sample mediamarks)])
AC_SUBST(GXINE_MISCPATH)

GXINE_CONFDIR="$sysconfdir/gxine"
eval GXINE_CONFPATH=`eval echo "$GXINE_CONFDIR"`
AC_DEFINE_UNQUOTED(GXINE_CONFDIR,"$GXINE_CONFPATH",[where to install conf files])
AC_SUBST(GXINE_CONFPATH)

AC_ARG_VAR([VENDOR_PKG_VERSION], [vendor build identifier string (default is the upstream version number, suffixed with "release" or identifying a snapshot type)])
POST_VERSION=release
PWD="`pwd`"
if test -d .hg || expr "`basename $PWD`" : 'gxine-[[0123456789abcdef]]\{12\}$' >/dev/null; then
  POST_VERSION='hg snapshot'
elif test -d CVS; then
  POST_VERSION='CVS snapshot'
elif expr "$PACKAGE_VERSION" : '.*-dev$' \| "$PACKAGE_VERSION" : '.*-dev-.*$' >/dev/null; then
  POST_VERSION='dev snapshot'
fi
VENDOR_PKG_VERSION="${VENDOR_PKG_VERSION:-$POST_VERSION}"
AC_MSG_NOTICE([using build identifier: "$VENDOR_PKG_VERSION"])
AC_DEFINE_UNQUOTED(VENDOR_PKG_VERSION,"$VENDOR_PKG_VERSION",[package build identifier, e.g. vendor name and package version])

AC_SUBST(MANLINGUAS,["de en es"])
AC_CONFIG_FILES([
Makefile
src/Makefile
browser-plugin/Makefile
pixmaps/Makefile
doc/Makefile
doc/man/Makefile
doc/man/de/Makefile
doc/man/en/Makefile
doc/man/es/Makefile
include/Makefile
m4/Makefile
m4/gettext/Makefile
misc/Makefile
misc/build_rpms.sh
misc/gxine.spec
misc/po/Makefile.in
po/Makefile.in
])

dnl doc/C/Makefile
AC_CONFIG_COMMANDS([default],
	[[chmod +x ./misc/build_rpms.sh; echo '
#ifdef DEBUG
# undef WITH_WATCHDOG
#endif' >> include/config.h
]],[[]])
AC_OUTPUT
