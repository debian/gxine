#! /bin/sh
test "$1" = '' || cd "$1"
PWD="`pwd`"
if test -d .hg; then
  HG_ID="`hg id 2>/dev/null | cut -d' ' -f1`"
elif expr "`basename $PWD`" : 'gxine-[[0123456789abcdef]]\{12\}$$' >/dev/null; then
  HG_ID="`echo $PWD | sed -e 's/^.*gxine-//'`"
fi
# extra quoting is needed for $(...) in makefile
HG_ID="#define CSET_ID \\\"${HG_ID+ }$HG_ID\\\""
echo "$HG_ID"
