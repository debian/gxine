/*
 * Copyright (C) 2001-2006 the xine project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * intl macros
 */

#ifndef GXINE_I18N_H
#define GXINE_I18N_H

#ifdef ENABLE_NLS
#    include <locale.h>
#    include <libintl.h>
#    define _(String) gettext (String)
#    ifdef gettext_noop
#        define N_(String) gettext_noop (String)
#    else
#        define N_(String) (String)
#    endif
#else
/* Stubs that do something close enough.  */
/* Make sure we don't duplicate xine-lib's versions of these macros */
#ifndef HAVE_XINEINTL_H
#define HAVE_XINEINTL_H
#    define textdomain(String) (String)
#    define gettext(String) (String)
#    define dgettext(Domain,Message) (Message)
#    define dcgettext(Domain,Message,Type) (Message)
#    define bindtextdomain(Domain,Directory) (Domain)
#    define ngettext(Singular,Plural,N) \
	    ((N) == 1 ? (Singular) : (Plural))
#    define dngettext(Domain,Singular,Plural,N) \
	    ((N) == 1 ? (Singular) : (Plural))
#    define dncgettext(Domain,Singular,Plural,N,Type) \
	    ((N) == 1 ? (Singular) : (Plural))
#    define bind_textdomain_codeset(Domain,Codeset) ((Codeset))
#endif

#    define _(String) (String)
#    define N_(String) (String)
#endif

#endif
