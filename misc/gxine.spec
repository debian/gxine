%define name      gxine
%define version	0.5.910
%define release	1
%define vendor packman

%define major   1
%define libname libxine%{major}
%define libvers 1

%define	ginstall install -c

Name:        %{name}
Summary:     GTK-based GUI for xine libraries
Summary(cs): Grafické rozhraní založené na GTK pro knihovny xine
Summary(de): GTK basierende grafische Oberfläche für die xine-Bibliotheken
Version:     %{version}
Release:     %{release}
License:     GPL
Group:       Applications/Multimedia
Source0:     %{name}-%{version}.tar.bz2
URL:         http://xine.sourceforge.net/
Packager:    Manfred Tremmel <Manfred.Tremmel@iiv.de>
Requires:    %{libname} >= %{libvers}
BuildPreReq: %{libname}-devel >= %{libvers}
BuildRoot:   %{_tmppath}/%{name}-buildroot

%description
This is a GTK+ based GUI for the libxine video player library.
It provides gxine, a media player that can play all the audio/video formats
that libxine supports. Currently, this includes MPEG1/2, some AVI and
Quicktime files, some network streaming methods and disc based media (VCD,
SVCD, DVD).
A more complete list can be found on http://www.xine-project.org/

Most DVDs on the market today are play-protected by the Content Scrambling
System (CSS). libxine does not provide any code to descramble those DVDs,
because of legal uncertainties. If you still want to play those DVDs, you'll
need a CSS decryption library like libdvdcss that is supported by libxine.

%description -l cs
Toto je grafické rozhraní založené na GTK+ pro libxine, knihovnu na přehrávání
videa. Balíček obsahuje gxine, multimediální přehrávač, který umí přehrát
všechny formáty zvuku a videa, které libxine podporuje. V současné době se
jedná o o MPEG1/2, některé soubory AVI a QuickTime, některé metody síťového
streamování a disková média (VCD, SVCD, DVD). Ucelenější seznam lze nalézt na
http://www.xine-project.org/

Většina DVD na trhu je dnes chráněna proti přehrávání Content Scrambling
systémem (CSS). libxine neobsahuje žádný kód k dekódování takových DVD, budete
potřebovat nějakou knihovnu na dešifrování CSS jako např. libdvdcss, jejíž
podpora v libxine zahrnuta je.

%description -l de
Dies ist eine GTK+ basierte GUI für die libxine Video Player Bibliothek.
Sie stellt gxine, einen Media Player zur Verfügung, welcher alle die Audio-
und Video-Formate abspielen kann, die von der libxine Bibliothek unterstützt
werden. Momentan beinhaltet dies MPEG 1/2, einige AVI und Quicktime Formate,
einige Netzwerk-Streaming-Metoden und Disk-basierende Medien (VCD, SVCD, DVD).

Eine vollständige Liste kann unter http://www.xine-project.org/ eingesehen
werden.

Die meisten erhältlichen DVDs sind mit dem "Content Scrambling System" (CSS)
technisch geschützt. Die libxine bringt keine Funktionalität mit, diese
DVDs zu entschlüsseln, da dies in vielen Ländern illegal ist. Sollten Sie
trotzdem solche DVDs abspielen wollen, benötigen Sie eine CSS
Entschlüsselungsbibliothek wie die libdvdcss, welche von der libxine
unterstützt wird.

%package browser-plugin
Summary:        gxine browser plugin
Summary(cs):    gxine modul prohlížeče
Summary(de):    gxine Browser Plugin
Group:          Productivity/Multimedia/Video/Players
Requires:       %{name} = %{version}-%{release}

%description browser-plugin
Browser plugin for Mozilla and other browsers using the same plugin infrastructure.

%description browser-plugin -l cs
Modul prohlížeče pro Mozillu a další prohlížeče používající stejnou
infrastrukturu modulů.

%description browser-plugin -l de
Browser plugin für Mozilla und andere Browser welche die selbe Plugin-Infrastruktur
verwenden.

%prep
%setup -n %{name}-%{version}

%build
export CFLAGS="${RPM_OPT_FLAGS}"

if [ ! -f configure ]; then
   NO_CONFIGURE=1 ./autogen.sh
   if [ ! -f mkinstalldirs ]; then
     cp %{_datadir}/automake-`rpm -q --queryformat '%{VERSION}' automake | cut -b 1-3`/mkinstalldirs .
   fi
fi

#
# currently we do not use %%configure as it seems to cause trouble with
# certain automake produced configure scripts - depending on automake version.
#
CFLAGS="$RPM_OPT_FLAGS" ./configure --build=%{_target_platform} --prefix=%{_prefix} \
	    --exec-prefix=%{_exec_prefix} --bindir=%{_bindir} \
	    --sbindir=%{_sbindir} --sysconfdir=%{_sysconfdir} \
	    --datadir=%{_datadir} --includedir=%{_includedir} \
	    --libdir=%{_libdir} --libexecdir=%{_libexecdir} \
	    --localstatedir=%{_localstatedir} \
	    --sharedstatedir=%{_sharedstatedir} --mandir=%{_mandir} \
	    --infodir=%{_infodir} \
	    VENDOR_PKG_VERSION='%{version}-%{release}; %{vendor}'
%{__make}

%install
[ "${RPM_BUILD_ROOT}" != "/" ] && %{__rm} -rf ${RPM_BUILD_ROOT}
%{__make} DESTDIR=${RPM_BUILD_ROOT} install
%{__mkdir_p} ${RPM_BUILD_ROOT}/%{_libdir}/browser-plugins
cd ${RPM_BUILD_ROOT}/%{_libdir}/browser-plugins/
%{__ln_s} ../gxine/gxineplugin.so .

%post browser-plugin
if [ "$1" = "1" ] || [ "$1" = "2" ] ; then # install or update
	# plugindirs to check
	PLUGINDIRS='/opt/mozilla/%{_lib}/plugins /opt/MozillaFirefox/%{_lib}/plugins /opt/netscape/plugins /opt/mozilla/plugins'
	# pluginname
	PLUGINNAME='gxineplugin.so'

	for PLUGINDIR in ${PLUGINDIRS}; do
		if [ -d ${PLUGINDIR} ]; then
			# check if plugin directory exists
			if [ ! -L ${PLUGINDIR}/${PLUGINNAME} ]; then
				# add a symlink if it doesn't exists
				cd ${PLUGINDIR}
				%{__ln_s} %{_libdir}/browser-plugins/${PLUGINNAME} .
				cd -
			fi
		fi
	done
fi

%preun browser-plugin
if [ "$1" = "0" ]; then # remove
	# plugindirs to check
	PLUGINDIRS='/opt/mozilla/%{_lib}/plugins /opt/MozillaFirefox/%{_lib}/plugins /opt/netscape/plugins /opt/mozilla/plugins'
	# pluginname
	PLUGINNAME='gxineplugin.so'

	for PLUGINDIR in ${PLUGINDIRS}; do
		if [ -d ${PLUGINDIR} ]; then
			# check if plugin directory exists
			if [ -L ${PLUGINDIR}/${PLUGINNAME} ]; then
				# remove symlink if it exists
				%{__rm} -f ${PLUGINDIR}/${PLUGINNAME}
			fi
		fi
	done
fi

%clean
[ "${RPM_BUILD_ROOT}" != "/" ] && %{__rm} -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root)
%doc AUTHORS BUGS ChangeLog COPYING INSTALL NEWS README README.cs README.de
%config(noreplace) %{_sysconfdir}/gxine/*
%{_bindir}/gxin*
%lang(de) %{_mandir}/de/man1/*
#%lang(fr) %{_mandir}/fr/man1/*
#%lang(es) %{_mandir}/es/man1/*
#%lang(pl) %{_mandir}/pl/man1/*
%{_mandir}/man1/*
%{_datadir}/applications/*
%{_datadir}/gxine/*
%{_datadir}/locale/*/LC_MESSAGES/gxine*.mo
%{_datadir}/pixmaps/gxine*
%{_datadir}/icons/hicolor/64x64/apps/*

%files browser-plugin
%defattr(-,root,root)
%{_libdir}/gxine/gxineplugin.*
%{_libdir}/browser-plugins/gxineplugin.*

%changelog
* Sun Aug 27 2006 František Dvořák <valtri@users.sourceforge.net>
- Czech translation
- added icon file
- switch the source from tar.gz to tar.bz2
* Tue Jul 4 2006 Darren Salt <dsalt@users.sourceforge.net>
- added doc file BUGS
* Sun Jan 8 2006 Darren Salt <dsalt@users.sourceforge.net>
- added doc files README.cs and README.de
* Mon Oct 17 2005 Darren Salt <dsalt@users.sourceforge.net>
- s/Copyright:/License:/; added %{_datadir}/pixmaps/gxine* to package
* Sun Sep 18 2005 Manfred Tremmel <Manfred.Tremmel@iiv.de>
- added pixmaps, * to LC_Messages and sysconfdir
- created a separate subrpm for browser-plugin
- some other little changes
* Thu Apr 28 2005 Darren Salt <dsalt@users.sourceforge.net>
- Add VENDOR_PKG_VERSION tagging and vendor name
* Thu Dec 16 2004 Manfred Tremmel <Manfred.Tremmel@iiv.de>
- desktop file switched from %{_datadir}/gnome/ to %{_datadir}/applications/
- replaced fix delete by deleting if base dir is not root
- make use of %{makeinstall} and %{__make}
* Sun Dec 12 2004 František Dvořák <valtri@users.sourceforge.net>
- mkinstalldirs not used anymore
* Sat Nov 11 2004 Manfred Tremmel <Manfred.Tremmel@iiv.de>
- some changes for gxine 0.4
- spec file now in UTF-8
- new description taken over from the debian package
* Sat Mar 01 2003 Manfred Tremmel <Manfred.Tremmel@iiv.de>
- Update to gxine 0.3
* Mon Dec 16 2002 Manfred Tremmel <Manfred.Tremmel@iiv.de>
- removed all packman specific and added to gnome-xine cvs
* Sat Dec 07 2002 Manfred Tremmel <Manfred.Tremmel@iiv.de>
- Update to gxine 0.2
* Thu Dec 05 2002 Manfred Tremmel <Manfred.Tremmel@iiv.de>
- initial release
