#!/bin/sh

DATE=`date +%y%m%d`
#DATE=1

# Some rpm checks
RPMVERSION=`rpm --version | tr [A-Z] [a-z] | sed -e 's/[a-z]//g' -e 's/\.//g' -e 's/ //g'`

# rpm version 4 return 40
if [ `expr $RPMVERSION` -lt 100 ]; then
  RPMVERSION=`expr $RPMVERSION \* 10`
fi

if [ `expr $RPMVERSION` -lt 400 ]; then
  RPM_BA="rpm -ba -ta ./gxine-0.5.910.tar.gz"
  RPM_BB="rpm -bb -ta ./gxine-0.5.910.tar.gz"
else
  RPM_BA="rpm -ta ./gxine-0.5.910.tar.gz -ba"
  RPM_BB="rpm -ta ./gxine-0.5.910.tar.gz -bb"
fi

echo "Creating tarball..."
rm -f config.cache && ./autogen.sh && make dist
rm -rf rpms
mkdir rpms

echo "*****************************************************"
echo
echo "building rpm for gxine 0.5.910"
echo
echo "current architecture:pentium"
echo "rpms will be copied to ./rpms directory"
echo
echo "*****************************************************"

export XINE_BUILD=i586-pc-linux-gnu

eval $RPM_BA

cp /usr/src/redhat/SRPMS/gxine-0.5.910-$DATE.src.rpm ./rpms/
mv /usr/src/redhat/RPMS/i386/gxine-0.5.910-$DATE.i386.rpm ./rpms/gxine-0.5.910-$DATE.i586.rpm

echo "Done."
