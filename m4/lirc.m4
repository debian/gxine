
dnl
dnl checking for lirc
dnl

AC_DEFUN([AC_CHECK_LIRC],
  [AC_ARG_ENABLE(lirc,
     [AS_HELP_STRING([--disable-lirc], [turn off LIRC support])],
     [given=Y], [given=N; enable_lirc=yes])

  found_lirc=no
  if test x"$enable_lirc" = xyes; then
    PKG_CHECK_MODULES(LIRC, liblircclient0, [found_lirc=yes], [:])
    if test "$found_lirc" = yes; then
      LIRC_INCLUDE="$LIRC_CFLAGS"
    else
     AC_REQUIRE_CPP
     AC_CHECK_LIB(lirc_client,lirc_init,
           [AC_CHECK_HEADER(lirc/lirc_client.h, [found_lirc=yes; LIRC_LIBS="-llirc_client"], found_lirc=no)], found_lirc=no)

     if test "$found_lirc" = no -a x"$cross_compiling" != xyes; then
        if test x"$LIRC_PREFIX" != "x"; then
           lirc_libprefix="$LIRC_PREFIX/lib"
	   LIRC_INCLUDE="-I$LIRC_PREFIX/include"
        fi
        for llirc in $lirc_libprefix /lib /usr/lib /usr/local/lib; do
          AC_CHECK_FILE(["$llirc/liblirc_client.so"],
             [LIRC_LIBS="$llirc/liblirc_client.so"]
             AC_DEFINE([HAVE_LIRC],,[Define this if you have LIRC (liblirc_client) installed]),
             AC_CHECK_FILE(["$llirc/liblirc_client.a"],
                [LIRC_LIBS="$llirc/liblirc_client.a"
                 found_lirc=yes],,)
          )
        done
     fi

     if test "$found_lirc" = "no"; then
	test $given = Y && AC_MSG_ERROR([LIRC client support requested but not available])
	AC_MSG_RESULT([*** LIRC client support not available, LIRC support will be disabled ***])
     fi
    fi
  fi
     if test "$found_lirc" = yes; then
	AC_DEFINE([HAVE_LIRC],1,[Define this if you have LIRC (liblirc_client) installed])
     fi
     AC_SUBST(LIRC_LIBS)
     AC_SUBST(LIRC_INCLUDE)
])


