dnl AC_C_ALWAYS_INLINE
dnl Define inline to something appropriate, including the new always_inline
dnl attribute from gcc 3.1
dnl Based on code by Michel LESPINASSE <walken@zoy.org>
dnl This variant leaves inline unchanged and defines always_inline.
AC_DEFUN([AC_C_ALWAYS_INLINE],
    [AC_C_INLINE
    if test x"$GCC" = x"yes" -a x"$ac_cv_c_inline" = x"inline"; then
	AC_MSG_CHECKING([for always_inline])
	dnl We know that GCC 3.3
	if expr match "`"$CC" -dumpversion`" '3\.4\.' >/dev/null; then
	    ac_cv_always_inline='yes, but gxine uses a construct on which GCC 3.4 fails'
	else
	    SAVE_CFLAGS="$CFLAGS"
	    CFLAGS="$CFLAGS -Wall -Werror"
	    AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[]], [[inline __attribute__ ((__always_inline__)) void f (void);]])],[ac_cv_always_inline=yes],[ac_cv_always_inline=no])
	    CFLAGS="$SAVE_CFLAGS"
	fi
	AC_MSG_RESULT([$ac_cv_always_inline])
	if test x"$ac_cv_always_inline" = x"yes"; then
	    always_inline='inline __attribute__ ((__always_inline__))'
	else
	    always_inline=inline
	fi
    else
	always_inline=''
    fi
    AC_DEFINE_UNQUOTED([ALWAYS_INLINE],[$always_inline],[use to force inlining (if possible)])
    ])

dnl Work around unneeded -l<lib> in some libraries' pkg-config files
dnl Intent is to minimise direct dependencies
dnl Assumption: --as-needed does nothing where all deps must be direct
AC_DEFUN([AC_LD_ASNEEDED],
    [SAVE_LDFLAGS="$LDFLAGS"
    LDFLAGS="$lt_prog_compiler_wl--as-needed $LDFLAGS"
    AC_MSG_CHECKING([whether the linker supports --as-needed])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[]], [[]])],[ac_cv_ld_asneeded=yes],[ac_cv_ld_asneeded=no])
    AC_MSG_RESULT([$ac_cv_ld_asneeded])
    if test "$ac_cv_ld_asneeded" = yes; then
      LDFLAGS="$lt_prog_compiler_wl-z ${lt_prog_compiler_wl}defs $LDFLAGS"
      AC_MSG_CHECKING([whether the linker supports -z defs])
      AC_LINK_IFELSE([AC_LANG_PROGRAM([[]], [[]])],[ac_cv_ld_z_defs=yes],[ac_cv_ld_z_defs=no])
      AC_MSG_RESULT([$ac_cv_ld_z_defs])
    fi
    test "$ac_cv_ld_asneeded" = yes && test "$ac_cv_ld_z_defs" = yes || LDFLAGS="$SAVE_LDFLAGS"
    ])

dnl OS-specific HAL device info lookup
dnl HAL_DEVICE_FILE_PROPERTY = HAL property giving the device pathname
dnl HAL_DEVICE_FILE_PREFIX   = Prefix for the device file property,
dnl                            e.g. "/dev/", were "/dev" omitted on Linux
dnl                            (note the trailing "/"!)
AC_DEFUN([GXINE_HAL_DEVICE_INFO],
    [AC_MSG_CHECKING([how to acquire device info via HAL])
    HAL_DEVICE_FILE_PROPERTY=''
    HAL_DEVICE_FILE_PREFIX=''
    case "$build_os" in
      linux-*)
	HAL_DEVICE_FILE_PROPERTY=linux.device_file
	;;
      kfreebsd-*)
	HAL_DEVICE_FILE_PROPERTY=freebsd.device_file
	;;
      solaris2.*)
	HAL_DEVICE_FILE_PROPERTY=solaris.devfs_path
	HAL_DEVICE_FILE_PREFIX=/devices/
	;;
    esac
    AC_MSG_RESULT(["$HAL_DEVICE_FILE_PROPERTY", prefix "$HAL_DEVICE_FILE_PREFIX"])
    if test "$HAL_DEVICE_FILE_PROPERTY" = ''; then
      AC_MSG_WARN([I don't know how to get HAL path info for $build_os.])
      AC_MSG_WARN([You won't see some info when viewing/selecting default devices.])
    else
      AC_DEFINE_UNQUOTED(HAL_DEVICE_FILE_PROPERTY, ["$HAL_DEVICE_FILE_PROPERTY"], [HAL device pathname property])
      if test "$HAL_DEVICE_FILE_PREFIX" != ''; then
        AC_DEFINE_UNQUOTED(HAL_DEVICE_FILE_PREFIX, ["$HAL_DEVICE_FILE_PREFIX"], [Common prefix for the HAL device pathname property])
      fi
    fi
    ])

dnl X11/XCB mix and match
AC_DEFUN([GXINE_CHECK_X11_XCB],
    [AC_ARG_WITH([xcb],
	    AS_HELP_STRING([--with-xcb], [build using XCB instead of Xlib]),
	    [if test "$withval" = no; then with_xcb=no; elif test "$withval" != soft; then with_xcb=yes; fi],
	    [with_xcb=soft])
    if test x"$with_xcb" != xno; then
      PKG_CHECK_MODULES([XCB], [xcb >= 1.0], [have_xcb=yes], [have_xcb=no])
      test x"$with_xcb" = xsoft || with_xcb=yes
      case "$have_xcb,$with_xcb" in
	no,yes)
	  AC_MSG_ERROR([XCB support requested but XCB not found])
	  ;;
	no,soft)
	  with_xcb=no
	  ;;
      esac
      if test x"$with_xcb" != xno; then
	AC_DEFINE(HAVE_XCB, 1, [Define if XCB is available and to be used])
        AC_ARG_ENABLE([x11-vo],
	    AS_HELP_STRING([--disable-x11-vo], [disable use of X11-based video output plugins]),
	    [if test "$withval" = no; then enable_x11_vo=no; else enable_x11_vo=yes; fi],
	    [enable_x11_vo=yes])
	if test x"$enable_x11_vo" != xno; then
	  AC_DEFINE(ENABLE_X11_VO, 1, [Define if X11 video output plugins are allowed with XCB])
	fi
      fi
    fi
    if test x"$with_xcb" = xno || test x"$enable_x11_vo" = xyes; then
      AC_PATH_XTRA
      if test x"$no_x" != xyes; then
        AC_DEFINE(HAVE_X11, 1, [Define if X11 is available and to be used])
      fi
    fi
    AM_CONDITIONAL([WITH_XCB], [test x"$with_xcb" != xno])
    AM_CONDITIONAL([WITH_X11], [test x"$no_x" != xyes])
    ])

dnl Screensaver binaries
dnl Args: config variable, #define variable prefix, binary name,
dnl       extra commands if found (optional)
AC_DEFUN([GXINE_SCREENSAVER_CMD],
   [if test "$$1" = yes; then
      $1=
    fi
    if test "$$1" != no; then
      AC_PATH_PROG([$1], [$3])
      if test -n "$$1"; then
        AC_DEFINE_UNQUOTED([$2_SCREENSAVER_COMMAND], ["$$1"], [location of the $3 binary])
	m4_default([$4], [])
	AC_DEFINE([SCREENSAVER_COMMANDS], [1], [we have at least one screensaver command])
      fi
    else
      AC_MSG_CHECKING([for $3])
      AC_MSG_RESULT([no])
    fi
    ])

dnl Shims for various functions which are present in newer xine-lib
AC_DEFUN([XINE_LIB_SHIMS],
   [AC_MSG_CHECKING([for re-entrant XML parser in xine-lib])
    tmp_CFLAGS="$CFLAGS"
    tmp_LIBS="$LIBS"
    CFLAGS="$CFLAGS $XINE_CFLAGS"
    LIBS="$LIBS $XINE_LIBS"
    AC_LINK_IFELSE(
	[AC_LANG_PROGRAM([[#include <xine/xmlparser.h>]],
			 [[xml_parser_init_r ((void *)0, 0, XML_PARSER_CASE_INSENSITIVE);]])],
	[AC_DEFINE([HAVE_XML_PARSER_REENTRANT], [[1]], [[Define if xml_parser_init_r etc. are available]])
	 AC_MSG_RESULT([[yes]])],
	[AC_MSG_RESULT([[no]])])
    CFLAGS="$tmp_CFLAGS"
    LIBS="$tmp_LIBS"
    ])
