dnl Check for Spidermonkey in several locations.

m4_define([spidermonkey_locate_header],
   [if test -f /usr/include/"$1$2"; then
	echo /usr/include/"$1"
    else
	echo /usr/local/include/"$1"
    fi
   ])

m4_define([spidermonkey_locate_lib],
   [case "$JS_CFLAGS" in
	-I/usr/local/include/*)
	  echo -L/usr/local/lib -l"$1"
	  ;;
	-I/usr/include/firefox/*)
	  echo -L/usr/lib/firefox -R/usr/lib/firefox -l"$1"
	  ;;
	*)
	  echo -l"$1"
	  ;;
    esac
   ])

m4_define([spidermonkey_locate],
    [AC_CHECK_HEADER([$1jsapi.h],
	[JS_CFLAGS="-I`spidermonkey_locate_header([$1], [jsapi.h])`"
	 JS_LIBS="`spidermonkey_locate_lib([$2])`"
	 if test "$3" = ""; then
	   HAVE_JS="lib$2"
	 else
	   HAVE_JS="lib$3"
	 fi
	],
	[unset JS_CFLAGS JS_LIBS],
	[#define XP_UNIX])
    ])

AC_DEFUN([GXINE_PATH_SPIDERMONKEY],
   [AC_ARG_WITH([spidermonkey],
		[AS_HELP_STRING([--with-spidermonkey],[prefix where libjs/libsmjs is installed (optional)])],
		[JS_CFLAGS="-I$withval"], [])

    AC_ARG_ENABLE([mozjs],
	[AS_HELP_STRING([--disable-mozjs],[don't look for Mozilla libmozjs])])
    AC_ARG_ENABLE([ffjs],
	[AS_HELP_STRING([--disable-ffjs], [don't look for Firefox libmozjs])])
    AC_ARG_ENABLE([seajs],
	[AS_HELP_STRING([--disable-seajs], [don't look for Seamonkey libmozjs])])

    if test x"$JS_CFLAGS" = x; then
      AC_MSG_NOTICE([looking for the Spidermonkey Javascript library in various places.])

      JSLIB=mozjs185
      AC_MSG_NOTICE([trying mozjs via pkgconfig mozjs185])
      PKG_CHECK_MODULES([JS],[mozjs185],[],[:])

      if test x"$JS_CFLAGS" = x; then
        JSLIB=mozjs
      fi

      # Try libmozjs (xulrunner)
      if test x"$JS_CFLAGS" = x; then
        HAVE_JS=xulrunner
        AC_MSG_NOTICE([trying mozjs via pkgconfig xulrunner-js])
        PKG_CHECK_MODULES([JS],[xulrunner-js],[],[:])
      fi

      # Try Mozilla

      if test x"$JS_CFLAGS" = x && test "$enable_mozjs" != "no"; then
        HAVE_JS=libmozjs
        AC_MSG_NOTICE([trying mozjs via pkgconfig mozilla-js])
	PKG_CHECK_MODULES([JS],[mozilla-js],[],[:])
	if test x"$JS_CFLAGS" != x; then
	  AC_MSG_WARN([gxine may fail if libmozjs.so is not in the usual locations. You may need to set LD_LIBRARY_PATH.])
	fi
      fi
      if test x"$JS_CFLAGS" = x && test "$enable_ffjs" != "no"; then
        HAVE_JS=libffjs
        AC_MSG_NOTICE([trying mozjs via pkgconfig firefox-js])
	PKG_CHECK_MODULES([JS],[firefox-js],[],[:])
	if test x"$JS_CFLAGS" != x; then
	  AC_MSG_WARN([gxine may fail if libmozjs.so is not in the usual locations. You may need to set LD_LIBRARY_PATH.])
	fi
      fi
      if test x"$JS_CFLAGS" = x && test "$enable_seajs" != "no"; then
	AC_MSG_NOTICE([trying mozjs via pkgconfig seamonkey-js])
	PKG_CHECK_MODULES([JS],[seamonkey-js],[],[:])
	if test x"$JS_CFLAGS" != x; then
	  AC_MSG_WARN([gxine may fail if libmozjs.so is not in the usual locations. You may need to set LD_LIBRARY_PATH.])
	fi
      fi

      if test x"$JS_CFLAGS" = x && test "$enable_mozjs" != "no"; then
        AC_MSG_NOTICE([trying mozjs, includes in /usr/include/mozilla/js])
	spidermonkey_locate([mozilla/js/], [mozjs])
      fi
      if test x"$JS_CFLAGS" = x && test "$enable_ffjs" != "no"; then
        AC_MSG_NOTICE([trying mozjs, includes in /usr/include/firefox/js])
	spidermonkey_locate([firefox/js/], [mozjs], [ffjs])
      fi
      if test x"$JS_CFLAGS" = x && test "$enable_ffjs" != "no"; then
	AC_MSG_NOTICE([trying mozjs, includes in /usr/include/firefox-1.5/js])
	spidermonkey_locate([firefox-1.5/js/], [mozjs], [ffjs])
      fi
      if test x"$JS_CFLAGS" = x && test "$enable_seajs" != "no"; then
	AC_MSG_NOTICE([trying mozjs, includes in /usr/include/seamonkey/js])
	spidermonkey_locate([seamonkey/js/], [mozjs])
      fi

      # Failed...

      if test x"$JS_CFLAGS" = x; then
	AC_MSG_ERROR([libjs not found])
      fi
    else
      saved_CFLAGS="$CFLAGS"
      CFLAGS="$CFLAGS $JS_CFLAGS"
      AC_MSG_NOTICE([using path $JS_CFLAGS for spidermonkey])
      AC_CHECK_HEADER([jsapi.h],
	[JS_LIBS="`case "$JS_CFLAGS" in
			*/smjs|*/smjs/)
			  echo sm
			  ;;
			*/mozilla/js|*/mozilla/js*|*/firefox/js|*/firefox/js*|*seamonkey/js|*seamonkey/js*)
			  echo moz
			  ;;
		   esac`"
         JSLIB="${JS_LIBS}js"
	 JS_LIBS="`spidermonkey_locate_lib([$JS_LIBS])`js"
	 AC_DEFINE(HAVE_LIBSMJS, 1, [Define if you have jsapi.h])
	],
	[AC_MSG_ERROR([libjs not found])],
	[#define XP_UNIX])
      CFLAGS="$saved_CFLAGS"
    fi

    SAVED_LIBS="$LIBS"
    LIBS="$JS_LIBS $LIBS"
    SAVED_CFLAGS="$CFLAGS"
    CFLAGS="$JS_CFLAGS $CFLAGS"
    AC_CHECK_LIB([$JSLIB], [JS_NewCompartmentAndGlobalObject],
      AC_DEFINE([HAVE_COMPARTMENTS], [1], [Define whether we have compartments]))

    AC_CHECK_LIB([$JSLIB], [JS_GetStringBytes],
      AC_DEFINE([HAVE_JS_GET_STRING_BYTES], [1], [Define whether we have JS_GetStringBytes]))

    AC_CHECK_LIB([$JSLIB], [JS_NewDouble],
      AC_DEFINE([HAVE_JS_NEW_DOUBLE], [1], [Define whether we have JS_NewDouble]))

    AC_CHECK_LIB([$JSLIB], [JS_StrictPropertyStub],
      AC_DEFINE([HAVE_JS_STRICT_PROPERTY_OP], [1], [Define whether we have the JSStrictPropertyOp prototype]))

    AC_COMPILE_IFELSE(
      [AC_LANG_PROGRAM(
        [[#include <jsapi.h>]],
        [[jsval v; jsdouble *d = JSVAL_TO_DOUBLE(v);]]
      )],
      AC_DEFINE([JSVAL_TO_DOUBLE_RETURNS_POINTER], [1], [Define whether the JSVAL_TO_DOUBLE function or macro returns a pointer to jsdouble])
    )

    CFLAGS="$JS_CFLAGS -Werror -Wno-attributes"
    AC_COMPILE_IFELSE(
      [AC_LANG_PROGRAM(
        [[#include <jsapi.h>
          static JSBool op(JSContext* cx, JSObject* obj, jsid id, jsval* vp) {}]],
        [[JSPropertyOp func = op;]]
      )],
      AC_DEFINE([JS_PROPERTY_OP_HAS_ID_AS_JSID], [1], [Define whether the type of id in the prototype for JSPropertyOp is a jsid])
    )
    LIBS="$SAVED_LIBS"
    CFLAGS="$SAVED_CFLAGS"

    AC_SUBST(JS_CFLAGS)
    AC_SUBST(JS_LIBS)
   ])

m4_define([nspr_locate],
    [AC_CHECK_HEADER([$1prtypes.h],
	[NSPR_CFLAGS="-I`spidermonkey_locate_header([$1], [prtypes.h])`"
	 NSPR_FOUND=y
	 break],
	[:], [])
    ])

AC_DEFUN([GXINE_PATH_NSPR],
   [NSPR_FOUND=n
    AC_MSG_NOTICE([looking for libnspr in various places.])
    dnl "name" via pkg-config
    dnl "/name/" via AC_CHECK_HEADER(name/prtypes.h ...)
    case "$HAVE_JS" in
      xulrunner)
	NSPR_CHECK='xulrunner-nspr nspr /nspr/'
	;;
      libffjs)
	NSPR_CHECK='firefox-nspr /firefox/nspr/ nspr /nspr/ mps /mps/'
	;;
      *)
	NSPR_CHECK='mozilla-nspr /mozilla/nspr/ nspr /nspr/'
	;;
    esac
    for nspr in $NSPR_CHECK; do
      if test "`echo $nspr | sed 's/^\(.\).*$/\1/'`" = '/'; then
	nspr_dir="`echo $nspr | sed 's/^.\(.*\)$/\1/g'`"
	nspr_locate(${nspr_dir})
      else
	AC_MSG_NOTICE([trying pkgconfig $nspr])
	PKG_CHECK_MODULES([NSPR],[$nspr],[NSPR_FOUND=y; break],[:])
      fi
    done
    if test "$NSPR_FOUND" = n; then
      AC_MSG_ERROR([nspr not found])
    fi
    AC_SUBST(NSPR_CFLAGS)
   ])

AC_DEFUN([GXINE_JS_CHECK_TYPES],
   [JSFLOAT64=n
    AC_MSG_CHECKING([for JSFloat64])
    SAVED_CFLAGS="$CFLAGS"
    CFLAGS="$JS_CFLAGS $CFLAGS"
    AC_COMPILE_IFELSE(
      [AC_LANG_PROGRAM(
        [[#include <jsapi.h>]],
        [[JSFloat64 test = 0.0;]]
      )],
      [AC_DEFINE([HAVE_JSFLOAT64], [1], [Define if JSFloat64 is declared])
       AC_MSG_RESULT([yes])],
      [AC_MSG_RESULT([no])],
    )
    CFLAGS="$SAVED_CFLAGS"
   ])
