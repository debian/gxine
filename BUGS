"The window's gone black!"

  Known cause: bad interaction with xfwm4 4.3.x when the compositor is
  enabled. To work around this, switch off Xv autopaint.
    File -> Configure -> Preferences: video.device.xv_colorkey

"I can't use F10 as a keybinding."

  Not a bug - hard-wiring of F10/Menu for menu popup can't be removed yet.
  It's currently possible for the input focus to be placed such that gxine
  has to handle these keys itself. References:
    * bugzilla.gnome.org bug 80432 (hard-wiring of F10 etc.) sourceforge bug
    * 1218956 (use of F10 within gxine)

  (gxine reads the gtkrc option "gtk-menu-bar-accel" and traps that instead
   of F10. But, at the time of writing, Shift-F10 is still hardwired in GTK.)

"gxine has stopped responding."

  This is quite likely to be a locking bug in gxine. You should reconfigure,
  adding --enable-lock-debugging and/or --enable-logging, then rebuild gxine.
  Both options cause *lots* of output to be generated.

  If you can provide a patch, so much the better...

"The window's just shrunk."

  There's a known issue here - occasionally, when the video aspect ration is
  changed, events are received in the "wrong" order, causing the video
  window to be incorrectly resized.

  Patches welcome :-)

"gxine just hung."

  You may (or may not) want to disable XCB support (--without-xcb). If you
  see something like _XReply showing up in the backtrace, then tell the X
  developers or use your distribution's bug tracker (depending on how you
  installed X). Details including which video output method gxine was using
  will be *very* helpful.

  If it's demonstrably a bug in gxine or xine-lib rather than in an X
  library, then tell us or use your distribution's bug tracker.
